package com.jonathan.cmp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;

/**
 * Created by salva on 27/05/2016.
 */
public class SitioWeb extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sitioturis);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);

       WebView wb=(WebView) findViewById(R.id.wbvInter);
        String url=getIntent().getStringExtra("url");
        Log.d("url: ", url);
        if(url.equals("1")){
            wb.loadUrl("http://www.estadiodefutbolmonterrey.com");
        }else if(url.equals("2")){
            wb.loadUrl("http://www.horno3.org");
        }else if(url.equals("3")){
            wb.loadUrl("http://www.parquefundidora.org");
        }else if(url.equals("4")) {
            wb.loadUrl("http://www.mexicodesconocido.com.mx/cerro-de-la-silla.html");
        }



    }
}
