package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 03/02/15.
 */
public class Launcher extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_FOTO = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/gallery/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);
    // albums JSONArray
    JSONArray albums = null;

    // albums JSON url
    private static final String URL_ALBUMS = "http://app.ecodsa.com.mx/APP/patrocinadores.php";

    // ALL JSON node names
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "cel";
    // Connection detector
    ConnectionDetector cd;

    // Progress Dialog

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();
    private static final long SPLASH_SCREEN_DELAY = 3000;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);


        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                // Replace the splash screen fragment to main fragment and
                // specific animation resources to run for the fragments that
                // are entering and exiting in this transaction.
                Intent ser = new Intent(getApplicationContext(), MyService.class);
                startService(ser);
              Intent intent = new Intent(getApplicationContext(), CMP.class);
               startActivity(intent);


                System.exit(0);
                onDestroy();
                finish();
                // Show action bar when the main fragment is visible
                runOnUiThread(new Runnable() {
                    public void run() {
                        getActionBar().show();
                    }
                });

            }

        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }



}


