package com.jonathan.cmp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.DialogInterface;
import android.content.OperationApplicationException;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class DecoderActivity extends Activity implements OnQRCodeReadListener {

    private TextView myTextView;
	private QRCodeReaderView mydecoderview;
	private ImageView line_image;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decoder);


        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);
        if ("phone".equals(tabletSize)) {
            // do something
            mydecoderview = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
            mydecoderview.setOnQRCodeReadListener(this, "phone");
        } else {
            // do something else
            mydecoderview = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
            mydecoderview.setOnQRCodeReadListener(this, "tablet");
        }
        
        myTextView = (TextView) findViewById(R.id.exampleTextView);
        
        line_image = (ImageView) findViewById(R.id.red_line_image);
        
        TranslateAnimation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f);
       mAnimation.setDuration(1000);
       mAnimation.setRepeatCount(-1);
       mAnimation.setRepeatMode(Animation.REVERSE);
       mAnimation.setInterpolator(new LinearInterpolator());
       line_image.setAnimation(mAnimation);
        
    }

    
    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed
	@Override
	public void onQRCodeRead(String text, PointF[] points) {

        try{
            if(!myTextView.getText().equals(text)) {

                myTextView.setText(text);
                String result = text;
                String[] arrayColores = result.split(";");
                Log.d("datos","dato"+result);

                final String clave = arrayColores[0];
                String[] arrayDatos = arrayColores[1].split(":");
                String[] arrayName = arrayDatos[0].split("TEL");
                final String nombre = arrayName[0];
                final String apellido = arrayDatos[1] + " " + arrayDatos[2];
                final String empresa = "";
                String[] arrayTe=arrayDatos[1].split("EMAIL");
                final String telefono = arrayTe[0];
                String[] arrayMail=arrayDatos[2].split("END");
                final String mail = arrayMail[0];
                final String nota = "Congreso Mexicano del Petróleo";

                AlertDialog.Builder builder = new AlertDialog.Builder(DecoderActivity.this);
                builder.setTitle(nombre + " " + apellido);
                builder.setMessage("¿Deseas guardar sus datos en tu dispositivo?")
                        .setCancelable(true)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                ArrayList<ContentProviderOperation> ops =
                                        new ArrayList<ContentProviderOperation>();

                                int rawContactID = ops.size();

                                // Adding insert operation to operations list
                                // to insert a new raw contact in the table ContactsContract.RawContacts
                                ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                                        .build());

                                // Adding insert operation to operations list
                                // to insert display name in the table ContactsContract.Data
                                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                                        .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, nombre)
                                        .build());

                                // Adding insert operation to operations list
                                // to insert display name in the table ContactsContract.Data
                                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                                        .withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, apellido)
                                        .build());

                                // Adding insert operation to operations list
                                // to  insert Home Phone Number in the table ContactsContract.Data
                                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                                        .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, empresa)
                                        .build());

                                // Adding insert operation to operations list
                                // to  insert Home Phone Number in the table ContactsContract.Data
                                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                        .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, telefono)
                                        .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK)
                                        .build());

                                // Adding insert operation to operations list
                                // to insert Work Email in the table ContactsContract.Data
                                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                                        .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, mail)
                                        .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                                        .build());

                                // Adding insert operation to operations list
                                // to insert Work Email in the table ContactsContract.Data
                                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE)
                                        .withValue(ContactsContract.CommonDataKinds.Note.NOTE, nota)
                                        .build());

                                try {
                                    // Executing all the insert operations as a single database transaction
                                    getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                                    Toast.makeText(getBaseContext(), "Los datos de " + nombre +  "se guardaron exitosamente", Toast.LENGTH_SHORT).show();
                                    finish();


                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                } catch (OperationApplicationException e) {
                                    e.printStackTrace();
                                }

                                             	/*
                                             	Intent i = new Intent(QKActivity.this, ResultActivity.class);
                                                 i.putExtra(ResultActivity.RESULT, rawResult.getText());
                                                 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                 startActivity(i);
                                                 */

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }

        }catch (Exception e){
            Toast.makeText(getBaseContext(), "No se puede leer este Codigo QR, verifique que se un Qr del congreso", Toast.LENGTH_SHORT).show();
            finish();
        }


    }

	
	// Called when your device have no camera
	@Override
	public void cameraNotFound() {
		
	}

	// Called when there's no QR codes in the camera preview image
	@Override
	public void QRCodeNotFoundOnCamImage() {
		
	}
    
	@Override
	protected void onResume() {
		super.onResume();
		mydecoderview.getCameraManager().startPreview();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mydecoderview.getCameraManager().stopPreview();
	}
}
