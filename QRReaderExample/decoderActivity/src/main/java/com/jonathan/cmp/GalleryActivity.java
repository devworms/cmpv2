package com.jonathan.cmp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 31/01/15.
 */
public class GalleryActivity extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app.ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    //
    private static final String MEDIA_PATH_FOTO = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Pictures/";
    private File filePhotoTwo = new File(MEDIA_PATH_FOTO);

    public Integer[] mThumbIds;

    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "nombre";

    public Uri pictureUri;

    private static final String URL_SONG = "http://app.ecodsa.com.mx/APP/patrocinadores.php";

    String id, name;
    ImageView iv;
    Bitmap imageBit ;

    ArrayList<String> albumsList = new ArrayList<String>();

    TextView mySelection;
    Gallery myGallery;
    ImageView imagenCompartir;
    CargaBanner cb= new CargaBanner();

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_gallery);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(GalleryActivity.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
          //  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        cd = new ConnectionDetector(getApplicationContext());

        // Check for internet connection
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(GalleryActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
       // new LoadBanners().execute();
        ImageView ImageTitu = (ImageView) findViewById(R.id.imvTitu);
        TextView Titulo = (TextView) findViewById(R.id.txtTitulo);

        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        Titulo.setTypeface(font);
        ImageTitu.setImageDrawable(this.getResources().getDrawable(R.drawable.seccion_fotos));
        new LoadSingleTrack().execute();

    }// onCreate
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };

    class LoadSingleTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(GalleryActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting song json and parsing
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            // post album id, song id as GET parameters
            params.add(new BasicNameValuePair("path", "img"));

            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_SONG, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", json);

            try {
                albums = new JSONArray(json);
                if(albums != null){
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);

                        String name = c.getString(TAG_NAME);
                        albumsList.add(name);
                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting song information
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    mySelection = (TextView) findViewById(R.id.mySelection);

                    // Bind the gallery defined in the main.xml
                    // Apply a new (customized) ImageAdapter to it.

                    if (!filePhotoTwo.exists()) {
                        filePhotoTwo.mkdirs();
                    }

                    myGallery = (Gallery) findViewById(R.id.myGallery);
                    myGallery.setAdapter(new ImageAdapter(GalleryActivity.this));
                    myGallery.setOnItemSelectedListener(new OnItemSelectedListener() {
                        private Context myContext;

                        String[] simpleArray = albumsList.toArray(new String[albumsList.size()]);

                        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                            mySelection.setText(" selected option: " + position );
                            imagenCompartir = (ImageView) findViewById(R.id.imageView);
                            imagenCompartir.setImageResource(Integer.parseInt(this.simpleArray[position]));

                            findViewById(R.id.btnLocalizar).setOnClickListener(new ActionButton1());
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            mySelection.setText("Nothing selected");
                        }
                    });




                    for (int i = 0; i < albumsList.size(); i++) {
                        System.out.println("Index: " + i + " - Item: " + albumsList.get(i));
                    }

                }
            });

        }
        private Bitmap downloadBitmap(String url) {
            Log.d("url img", url);
            // initilize the default HTTP client object
            final DefaultHttpClient client = new DefaultHttpClient();

            //forming a HttoGet request
            final HttpGet getRequest = new HttpGet(url);
            try {

                HttpResponse response = client.execute(getRequest);

                //check 200 OK for success
                final int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode != HttpStatus.SC_OK) {
                    Log.w("ImageDownloader", "Error " + statusCode +
                            " while retrieving bitmap from " + url);
                    return null;

                }

                final HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream inputStream = null;
                    try {
                        // getting contents from the stream
                        inputStream = entity.getContent();

                        // decoding stream data back into image Bitmap that android understands
                        imageBit = BitmapFactory.decodeStream(inputStream);


                    } finally {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        entity.consumeContent();
                    }
                }
            } catch (Exception e) {
                // You Could provide a more explicit error message for IOException
                getRequest.abort();
                Log.e("ImageDownloader", "Something went wrong while" +
                        " retrieving bitmap from " + url + e.toString());
            }

            return imageBit;
        }

    }



    class ActionButton1 implements View.OnClickListener {
        public void onClick(View v) {


        }
    }

    public class ImageAdapter extends BaseAdapter {
        /** The parent context */
        private Context myContext;
        // Put some images to project-folder: /res/drawable/
        // format: jpg, gif, png, bmp, ...
        private int[] myImageIds = { R.drawable.uno, R.drawable.dos,
                R.drawable.tres, R.drawable.cuatro };

        /** Simple Constructor saving the 'parent' context. */
        public ImageAdapter(Context c) {
            this.myContext = c;
        }

        // inherited abstract methods - must be implemented
        // Returns count of images, and individual IDs
        public int getCount() {
            return this.myImageIds.length;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
        // Returns a new ImageView to be displayed,
        public View getView(int position, View convertView,
                            ViewGroup parent) {

            // Get a View to display image data
            ImageView iv = new ImageView(this.myContext);
            iv.setImageResource(this.myImageIds[position]);

            // Image should be scaled somehow
            //iv.setScaleType(ImageView.ScaleType.CENTER);
            //iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            //iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
            //iv.setScaleType(ImageView.ScaleType.FIT_XY);
            iv.setScaleType(ImageView.ScaleType.FIT_END);

            // Set the Width & Height of the individual images
            iv.setLayoutParams(new Gallery.LayoutParams(115, 90));

            return iv;
        }
    }// ImageAdapter

    class LoadBanners extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("path", "publicidad"));
            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_BANNERS, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", "> " + json);

            try {
                albums = new JSONArray(json);

                if (albums != null) {
                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);


                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        String tabletSize = getResources().getString(R.string.screen_type);
                        Log.d("pantalla", tabletSize);
                        if ("phone".equals(tabletSize)) {
                            // do something
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_BANNER);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app.ecodsa.com.mx/publicidadc/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        } else {
                            // do something else
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_TABLET);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER_TABLET, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app.ecodsa.com.mx/publicidadt/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        }

                        //ImageView imgView = (ImageView)findViewById(R.id.Banner);

                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {


                    final String[] simpleArray = BannerList.toArray(new String[BannerList.size()]);
                    final String[] LinksimpleArray = LinkBannerList.toArray(new String[LinkBannerList.size()]);
                    imageButton = (ImageButton) findViewById(R.id.Banner);


                    final Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        int i=0;
                        public void run() {


                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int width = size.x;

                            String tabletSize = getResources().getString(R.string.screen_type);
                            Log.d("pantalla", tabletSize);
                            if ("phone".equals(tabletSize)) {
                                // do something
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/1080 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                // do something else
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/2048 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            }

                            i++;
                            if(i>simpleArray.length-1)
                            {
                                i=0;
                            }
                            handler.postDelayed(this, 5000);  //for interval...
                        }
                    };
                    handler.postDelayed(runnable, 1000); //for initial delay..
                }
            });

        }



        private void downloadImagesToSdCard(String downloadUrl,String imageName)
        {
            try{
                URL url = new URL(downloadUrl); //you can write here any link

                File myDir =  new File(MEDIA_PATH_BANNER);
                //Something like ("/sdcard/file.mp3")


                if(!myDir.exists()){
                    myDir.mkdir();
                    Log.v("", "inside mkdir");

                }

                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = imageName;
                File file = new File(myDir, fname);
                if (file.exists ()) file.delete ();

             /* Open a connection to that URL. */
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection)ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

            /*
             * Define InputStreams to read from the URLConnection.
             */
                // InputStream is = ucon.getInputStream();
            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */

                FileOutputStream fos = new FileOutputStream(file);
                int size = 1024*1024;
                byte[] buf = new byte[size];
                int byteRead;
                while (((byteRead = inputStream.read(buf)) != -1)) {
                    fos.write(buf, 0, byteRead);
                    int bytesDownloaded = byteRead;
                }
            /* Convert the Bytes read to a String. */

                fos.close();

            }catch(IOException io)
            {
                io.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

    }

}// AndDemoUI