package com.jonathan.cmp;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 01/02/15.
 */




public class GridViewActivity extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/gallery/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    //
    private static final String MEDIA_PATH_FOTO = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/gallery/";
    private File filePhotoTwo = new File(MEDIA_PATH_FOTO);
    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    String tabla;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    public ArrayList albumsList;
    ByteArrayOutputStream bs;
    // albums JSONArray
    JSONArray albums = null;
    Bitmap imgb1,imgb2,imgb3,imgb4,imgb5,imgb6,imgb7,imgb8,imgb9,imgb10,imgb11,imgb12;
    ImageView img1,img2,img3,img4,img5,img6,img7,img8,img9,img10,img11,img12;
    int c,numeroImg;
    Bitmap imagenFoto;
    public ImageView imgSelect;
    // albums JSON url
    private static final String URL_ALBUMS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";

    // ALL JSON node names
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "nombre";
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_layout);


        String tabletSize = getResources().getString(R.string.screen_type);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(GridViewActivity.this));
        Log.d("pantallaFOTO", tabletSize);
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        if (!filePhotoTwo.exists()) {
            filePhotoTwo.mkdirs();
        }

        cd = new ConnectionDetector(getApplicationContext());

        // Check for internet connection
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(GridViewActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
        TextView Titulo = (TextView) findViewById(R.id.txtTitulo);

        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        Titulo.setTypeface(font);
        ImageView ImageTitu = (ImageView) findViewById(R.id.imvTitu);
        ImageTitu.setImageDrawable(this.getResources().getDrawable(R.drawable.seccion_fotos));
        conta();
        img1= (ImageView) findViewById(R.id.imgFotos1);
        img2= (ImageView) findViewById(R.id.imgFotos2);
        img3= (ImageView) findViewById(R.id.imgFotos3);
        img4= (ImageView) findViewById(R.id.imgFotos4);
        img5= (ImageView) findViewById(R.id.imgFotos5);
        img6= (ImageView) findViewById(R.id.imgFotos6);
        img7= (ImageView) findViewById(R.id.imgFotos7);
        img8= (ImageView) findViewById(R.id.imgFotos8);
        img9= (ImageView) findViewById(R.id.imgFotos9);
        img10= (ImageView) findViewById(R.id.imgFotos10);
        img11= (ImageView) findViewById(R.id.imgFotos11);
        img12= (ImageView) findViewById(R.id.imgFotos12);
        new LoadAlbums().execute();
        //img1.setImageBitmap(getBitmapFromURL("http://app-ecodsa.com.mx/GALERIA/08/01.jpg"));
      //  new LoadBanners().execute();
        // Loading Albums JSON in Background Thread


        // get listview


    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    public void mostrarimage(int i){


        Intent intent = new Intent(GridViewActivity.this, FotoSelec.class);


        numeroImg=i;



        if(numeroImg==1){
             bs = new ByteArrayOutputStream();
            imgb1.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==2){
             bs = new ByteArrayOutputStream();
            imgb2.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==3){
             bs = new ByteArrayOutputStream();
            imgb3.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==4){
             bs = new ByteArrayOutputStream();
            imgb4.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==5){
             bs = new ByteArrayOutputStream();
            imgb5.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==6){
             bs = new ByteArrayOutputStream();
            imgb6.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==7){
             bs = new ByteArrayOutputStream();
            imgb7.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==8){
             bs = new ByteArrayOutputStream();
            imgb8.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==9){
             bs = new ByteArrayOutputStream();
            imgb9.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==10){
             bs = new ByteArrayOutputStream();
            imgb10.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==11){
             bs = new ByteArrayOutputStream();
            imgb11.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }else if(numeroImg==12){
             bs = new ByteArrayOutputStream();
            imgb12.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        }
        intent.putExtra("byteArray", bs.toByteArray());
        intent.putExtra("numeroImg",numeroImg);
        startActivity(intent);

       /* final Dialog dialog = new Dialog(this);
        //Aqui haces que tu layout se muestre como dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_fotoselect);


        numeroImg=i;
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Button btnGuard= (Button) dialog.findViewById(R.id.btn_Descarga);
        btnGuard.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d("FOTO","numero"+imagenFoto);


                if(numeroImg==1){
                    imagenFoto = ((BitmapDrawable)img1.getDrawable()).getBitmap();
                }else if(numeroImg==2){
                    imagenFoto = ((BitmapDrawable)img2.getDrawable()).getBitmap();
                }else if(numeroImg==3){
                    imagenFoto = ((BitmapDrawable)img3.getDrawable()).getBitmap();
                }else if(numeroImg==4){
                    imagenFoto = ((BitmapDrawable)img4.getDrawable()).getBitmap();
                }else if(numeroImg==5){
                    imagenFoto = ((BitmapDrawable)img5.getDrawable()).getBitmap();
                }else if(numeroImg==6){
                    imagenFoto = ((BitmapDrawable)img6.getDrawable()).getBitmap();
                }else if(numeroImg==7){
                    imagenFoto = ((BitmapDrawable)img7.getDrawable()).getBitmap();
                }else if(numeroImg==8){
                    imagenFoto = ((BitmapDrawable)img8.getDrawable()).getBitmap();
                }else if(numeroImg==9){
                    imagenFoto = ((BitmapDrawable)img9.getDrawable()).getBitmap();
                }else if(numeroImg==10){
                    imagenFoto = ((BitmapDrawable)img10.getDrawable()).getBitmap();
                }else if(numeroImg==11){
                    imagenFoto = ((BitmapDrawable)img11.getDrawable()).getBitmap();
                }else if(numeroImg==12){
                    imagenFoto = ((BitmapDrawable)img12.getDrawable()).getBitmap();
                }


                String ruta = guardarImagen(getApplicationContext(), "imagen"+ numeroImg, imagenFoto);

                Toast.makeText(getApplicationContext(), "Imagen guardada correctamente", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }

        });

        imgSelect= (ImageView)  dialog.findViewById(R.id.imgDialogImage);
        if(i==1) {
            imgSelect.setImageBitmap(imgb1);
        }else if(i==2){
            imgSelect.setImageBitmap(imgb2);
        }else if(i==3){
            imgSelect.setImageBitmap(imgb3);
        }else if(i==4){
            imgSelect.setImageBitmap(imgb4);
        }else if(i==5){
            imgSelect.setImageBitmap(imgb5);
        }else if(i==6){
            imgSelect.setImageBitmap(imgb6);
        }else if(i==7){
            imgSelect.setImageBitmap(imgb7);
        }else if(i==8){
            imgSelect.setImageBitmap(imgb8);
        }else if(i==9){
            imgSelect.setImageBitmap(imgb9);
        }else if(i==10){
            imgSelect.setImageBitmap(imgb10);
        }else if(i==11){
            imgSelect.setImageBitmap(imgb11);
        }else if(i==12){
            imgSelect.setImageBitmap(imgb12);
        }



        dialog.show();*/
    }

    /**
     * Background Async Task to Load all Albums by making http request
     * */
    class LoadAlbums extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(GridViewActivity.this);
            pDialog.setMessage("Listing...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            Log.d("Entro:", "si");
            try {
                c=0;
                for (int i=1;i<=12;i++){

                    Log.d("foto:", "f"+i);
                    URL url = new URL("http://app-ecodsa.com.mx/GALERIA/08/0"+i+".jpg");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    if(i==1) {
                        imgb1 = BitmapFactory.decodeStream(input);
                    }else if(i==2){
                        imgb2 = BitmapFactory.decodeStream(input);
                    }else if(i==3){
                        imgb3 = BitmapFactory.decodeStream(input);
                    }else if(i==4){
                        imgb4 = BitmapFactory.decodeStream(input);
                    }else if(i==5){
                        imgb5 = BitmapFactory.decodeStream(input);
                    }else if(i==6){
                        imgb6 = BitmapFactory.decodeStream(input);
                    }else if(i==7){
                        imgb7 = BitmapFactory.decodeStream(input);
                    }else if(i==8){
                        imgb8 = BitmapFactory.decodeStream(input);
                    }else if(i==9){
                        imgb9 = BitmapFactory.decodeStream(input);
                    }else if(i==10){
                        imgb10 = BitmapFactory.decodeStream(input);
                    }else if(i==11){
                        imgb11 = BitmapFactory.decodeStream(input);
                    }else if(i==12){
                        imgb12 = BitmapFactory.decodeStream(input);
                    }
                    c=c+1;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            pDialog.setMessage("Listing..." + c * 8+"%");
                        }
                    });

                }


            } catch (IOException e) {


            }
            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    pDialog.setMessage("Listing...100%");
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    img1.setImageBitmap(imgb1);
                    img2.setImageBitmap(imgb2);
                    img3.setImageBitmap(imgb3);
                    img4.setImageBitmap(imgb4);
                    img5.setImageBitmap(imgb5);
                    img6.setImageBitmap(imgb6);
                    img7.setImageBitmap(imgb7);
                    img8.setImageBitmap(imgb8);
                    img9.setImageBitmap(imgb9);
                    img10.setImageBitmap(imgb10);
                    img11.setImageBitmap(imgb11);
                    img12.setImageBitmap(imgb12);

                    img1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "foto"+imgb1.toString());
                            mostrarimage(1);


                        }
                    });
                    img2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(2);

                        }
                    });
                    img3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(3);

                        }
                    });
                    img4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(4);

                        }
                    });
                    img5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(5);

                        }
                    });
                    img6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(6);

                        }
                    });
                    img7.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(7);

                        }
                    });
                    img8.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(8);

                        }
                    });
                    img9.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(9);

                        }
                    });
                    img10.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(10);

                        }
                    });
                    img11.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(11);

                        }
                    });
                    img12.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("FOTO", "1");
                            mostrarimage(12);

                        }
                    });

                    pDialog.dismiss();
                }
            });

        }





    }


    private Bitmap descargarImagen (String imageHttpAddress){
        URL imageUrl = null;
        Bitmap imagen = null;
        try{
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            imagen = BitmapFactory.decodeStream(conn.getInputStream());
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return imagen;
    }






}