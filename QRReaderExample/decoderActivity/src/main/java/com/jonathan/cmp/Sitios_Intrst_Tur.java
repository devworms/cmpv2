package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by mgarza on 12/04/2016.
 */
public class Sitios_Intrst_Tur extends  Activity {
    CargaBanner cb= new CargaBanner();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sitios_intrst_tur);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(Sitios_Intrst_Tur.this));
        conta();
        Intent i = getIntent();
        String titu= i.getStringExtra("titulo");
        TextView txtTtulo=(TextView)findViewById(R.id.txtTitulo);
        txtTtulo.setText(titu);

        TextView txtSitio1=(TextView)findViewById(R.id.txtEsta);
        TextView txtSitio2=(TextView)findViewById(R.id.txtHorn);
        TextView txtSitio3=(TextView)findViewById(R.id.txtPark);
        TextView txtSitio4=(TextView)findViewById(R.id.txtCer);

        txtSitio1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent sitio = new Intent(getApplicationContext(), SitioWeb.class);
                sitio.putExtra("url", "1");
                startActivity(sitio);


            }

        });
        txtSitio2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent sitio = new Intent(getApplicationContext(), SitioWeb.class);
                sitio.putExtra("url", "2");
                startActivity(sitio);


            }

        });
        txtSitio3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent sitio = new Intent(getApplicationContext(), SitioWeb.class);
                sitio.putExtra("url", "3");
                startActivity(sitio);


            }

        });
        txtSitio4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent sitio = new Intent(getApplicationContext(), SitioWeb.class);
                sitio.putExtra("url", "4");
                startActivity(sitio);


            }

        });

        // 1) How to replace link by text like "Click Here to visit Google" and
        // the text is linked with the website url ?

    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };

}
