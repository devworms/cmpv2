package com.jonathan.cmp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by sergio on 12/05/16.
 */
public class PushProgramada {

    private Context contexto;
    public PushProgramada(Context contexto){
        this.contexto = contexto;
    }

    public void notify(String titulo, String cuerpo) {
        String name = this.getClass().getName();

        Intent notificationIntent = new Intent(contexto, Launcher.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(contexto, 0,
                notificationIntent, 0);
        Notification notification = new Notification.Builder(this.contexto)
                .setContentTitle(titulo).setAutoCancel(true)
                .setSmallIcon(R.drawable.app_icon).setContentIntent(intent)
                .setContentText(cuerpo).build();


        NotificationManager notificationManager =
                (NotificationManager) this.contexto.getSystemService(this.contexto.NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), notification);
    }
}

