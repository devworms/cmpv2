package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;

/**
 * Created by lhuu on 30/01/15.
 */
public class FiltroMapaENG extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app.ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    //new LoadBanners().execute();
    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    private Spinner spinner1, spinner2;
    private Button btnSubmit;

    String TITULO;
    String tabla;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_program_encuesta);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

       // new LoadBanners().execute();
        addItemsOnSpinner1();


        Intent i = getIntent();
        TITULO = i.getStringExtra("TITULO");

        Log.d("TITULO:", TITULO);

        TextView titulo=(TextView) findViewById(R.id.txtTitulo);
        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        titulo.setTypeface(font);
        TextView Descripcion = (TextView) findViewById(R.id.txtDescrip);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        titulo.setText(TITULO);
        Descripcion.setText("Explore maps to locate your favorite events and visit our exhibitors.");
        btnSubmit.setText("SEARCH");
    }

    // add items into spinner dynamically
    public void addItemsOnSpinner1() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        List<String> list = new ArrayList<String>();
        list.add("Mapa Recinto");
        list.add("Mapa Expositores");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
        spinner1.setPrompt("maps type");spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if ("Mapa Recinto".equals(spinner1.getSelectedItem())) {
                    addItemsOnSpinner3();
                } else {
                    addItemsOnSpinner2();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }// add items into spinner dynamically
    public void addItemsOnSpinner2() {

        spinner2 = (Spinner) findViewById(R.id.spinner2);
        List<String> list = new ArrayList<String>();
        list.add("Expositor");
        list.add("Patrocinador");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter);
        spinner2.setPrompt("maps content");

        addListenerOnButton();
    }


    // add items into spinner dynamically
    public void addItemsOnSpinner3() {

        spinner2 = (Spinner) findViewById(R.id.spinner2);
        List<String> list = new ArrayList<String>();
        list.add("Programa general");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter);
        spinner2.setPrompt("maps content");
        addListenerOnButton();
    }

    // get the selected dropdown list value
    public void addListenerOnButton() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if("Programa general".equals(spinner2.getSelectedItem())){
                    tabla = "program_mapa";
                } else if ("Expositor".equals(spinner2.getSelectedItem())){
                    tabla = "expositor_mapa";
                } else if ("Patrocinador".equals(spinner2.getSelectedItem())){
                    tabla = "patrocinador_mapa";
                }


                Intent intent = new Intent(getApplicationContext(), ListadoActivityENG.class);
                intent.putExtra("TITULO", TITULO);
                intent.putExtra("tabla", tabla);
                intent.putExtra("fecha", "");
                intent.putExtra("tipo", "");
                startActivity(intent);

            }

        });
    }

    class LoadBanners extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("path", "publicidad"));
            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_BANNERS, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", "> " + json);

            try {
                albums = new JSONArray(json);

                if (albums != null) {
                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);


                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        String tabletSize = getResources().getString(R.string.screen_type);
                        Log.d("pantalla", tabletSize);
                        if ("phone".equals(tabletSize)) {
                            // do something
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_BANNER);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app.ecodsa.com.mx/publicidadc/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        } else {
                            // do something else
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_TABLET);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER_TABLET, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app.ecodsa.com.mx/publicidadt/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        }

                        //ImageView imgView = (ImageView)findViewById(R.id.Banner);

                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {


                    final String[] simpleArray = BannerList.toArray(new String[BannerList.size()]);
                    final String[] LinksimpleArray = LinkBannerList.toArray(new String[LinkBannerList.size()]);
                    imageButton = (ImageButton) findViewById(R.id.Banner);


                    final Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        int i=0;
                        public void run() {


                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int width = size.x;

                            String tabletSize = getResources().getString(R.string.screen_type);
                            Log.d("pantalla", tabletSize);
                            if ("phone".equals(tabletSize)) {
                                // do something
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/1080 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                // do something else
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/2048 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            }

                            i++;
                            if(i>simpleArray.length-1)
                            {
                                i=0;
                            }
                            handler.postDelayed(this, 5000);  //for interval...
                        }
                    };
                    handler.postDelayed(runnable, 1000); //for initial delay..
                }
            });

        }



        private void downloadImagesToSdCard(String downloadUrl,String imageName)
        {
            try{
                URL url = new URL(downloadUrl); //you can write here any link

                File myDir =  new File(MEDIA_PATH_BANNER);
                //Something like ("/sdcard/file.mp3")


                if(!myDir.exists()){
                    myDir.mkdir();
                    Log.v("", "inside mkdir");

                }

                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = imageName;
                File file = new File(myDir, fname);
                if (file.exists ()) file.delete ();

             /* Open a connection to that URL. */
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection)ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

            /*
             * Define InputStreams to read from the URLConnection.
             */
                // InputStream is = ucon.getInputStream();
            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */

                FileOutputStream fos = new FileOutputStream(file);
                int size = 1024*1024;
                byte[] buf = new byte[size];
                int byteRead;
                while (((byteRead = inputStream.read(buf)) != -1)) {
                    fos.write(buf, 0, byteRead);
                    int bytesDownloaded = byteRead;
                }
            /* Convert the Bytes read to a String. */

                fos.close();

            }catch(IOException io)
            {
                io.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

    }
}