package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by mgarza on 12/04/2016.
 */

    /**
     * Created by mgarza on 07/04/2016.
     */
    public class Sitios_Instrst extends Activity implements View.OnClickListener {
        CargaBanner cb= new CargaBanner();

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sitios_intrst);

            ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
            imgbbanner.setImageDrawable(cb.cargaimage(Sitios_Instrst.this));
            conta();
            Button btnTuristicos = (Button) findViewById(R.id.btnSITuristicos);
            btnTuristicos.setOnClickListener(this); // calling onClick() method

            Button btnEmergencia = (Button) findViewById(R.id.btnSIEmergencia);
            btnEmergencia.setOnClickListener(this);// calling onClick() method


        }
        public void conta(){
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(timerTask, 0, 3000);
        }
        TimerTask timerTask = new TimerTask()
        {
            public void run()
            {
                runOnUiThread(new Runnable() {
                    public void run() {
                        ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                        imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                    }
                });

            }
        };
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.btnSITuristicos:
                    intent  = new Intent(getApplicationContext(), Sitios_Intrst_Tur.class);
                    intent.putExtra("titulo","TURISTICOS");
                    startActivity(intent);
                    break;
                case R.id.btnSIEmergencia:
                     intent = new Intent(getApplicationContext(), Sitios_Intrst_eme.class);
                    intent.putExtra("titulo","EMERGENCIAS");
                    startActivity(intent);
                    break;
               }

        }

    }

