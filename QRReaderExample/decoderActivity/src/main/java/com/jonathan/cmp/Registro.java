package com.jonathan.cmp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.Response;
import com.jonathan.cmp.mobile.AWSMobileClient;
import com.jonathan.cmp.mobile.user.IdentityManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 05/02/15.
 */
public class Registro extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private static final String MEDIA_PATH_REGISTRO = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/Regis/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);
    String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    ConnectionDetector cd;

    /** Class name for log messages. */
    private final static String LOG_TAG = CMP.class.getSimpleName();

    private IdentityManager identityManager;

    HttpClient httpclient ;
    HttpPost httppost ;
    private ArrayList<NameValuePair> nameValuePairs;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;


    EditText numedit;
    EditText apellidoedit;
    EditText emailedit;
    Spinner spinner1;
    Chronometer crono;
    private static String LENGUAJE = "";
    CargaBanner cb= new CargaBanner();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        String tabletSize = getResources().getString(R.string.screen_type);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(Registro.this));
        Log.d("pantalla", tabletSize);


        //imgbbanner.setImageDrawable(cb.cargaimage(Registro.this));



        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(Registro.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
       // new LoadBanners().execute();

        TextView titulo = (TextView) findViewById(R.id.txtTitulo);
        Button Registrar = (Button) findViewById(R.id.btnSubmit);
        TextView pasar = (TextView) findViewById(R.id.txtPasarSinCon);

        numedit = (EditText) findViewById(R.id.editText);


        LENGUAJE = Locale.getDefault().getDisplayLanguage();
        Log.d("Lenguaje:", LENGUAJE);

        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
            titulo.setText("INGRESO DE USUARIO");
            //numedit.setHint("");


            Registrar.setText("INGRESAR");
        } else {
            titulo.setText("NEW USER REGISTRATION");
            Registrar.setText("ENTER");
            pasar.setText("ENTER WITHOUT REGISTRATION");
            numedit.setHint("Registration Number");
        }

        conta();
        pasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), CMP.class);
                intent.putExtra("paso","1");
                startActivity(intent);


                System.exit(0);
                onDestroy();
                finish();
            }
        });

        findViewById(R.id.btnSubmit).setOnClickListener(new ActionButton1());


        // Obtain a reference to the mobile client. It is created in the Application class,
        // but in case a custom Application class is not used, we initialize it here if necessary.
        AWSMobileClient.initializeMobileClientIfNecessary(this);

        // Obtain a reference to the mobile client. It is created in the Application class.
        final AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        // Obtain a reference to the identity manager.
        identityManager = awsMobileClient.getIdentityManager();

    }

    @Override
    protected void onResume() {
        super.onResume();


        final AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        // register notification receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(notificationReceiver,
                new IntentFilter(PushListenerService.ACTION_SNS_NOTIFICATION));
    }

    private final BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(LOG_TAG, "Received notification from local broadcast. Display it in a dialog.");

            Bundle data = intent.getBundleExtra(PushListenerService.INTENT_SNS_NOTIFICATION_DATA);
            String message = PushListenerService.getMessage(data);

            new AlertDialog.Builder(Registro.this)
                    .setTitle(getString(R.string.push_title))
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        }
    };

    @Override
    protected void onPause() {
        super.onPause();

        // unregister notification receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationReceiver);
    }

    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };

    class LoadAlbums extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {

        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                }
            });

        }

    }



    class ActionButton1 implements View.OnClickListener {
        public void onClick(View v) {
            if(numedit.getText().toString().equals("master-cmp")){
                Intent intent = new Intent(getApplicationContext(), CMP.class);
                intent.putExtra("paso","2");
                startActivity(intent);


                System.exit(0);
                onDestroy();
                finish();
            }
            else{
                httpclient = new DefaultHttpClient();
                httppost = new HttpPost("http://app-ecodsa.com.mx/APP/login.php");

                try { // make sure the url is correct.
                    //add your data



                    nameValuePairs = new ArrayList<NameValuePair>(2);
                    // Always use the same variable name for posting i.e the android side variable name and php side variable name should be similar,
                    nameValuePairs.add(new BasicNameValuePair("codigo_registro", numedit.getText().toString()));  // $Edittext_value = $_POST['Edittext_value'];



                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    // Execute HTTP Post Request
                    HttpResponse response = httpclient.execute(httppost);

                    int responseCode = response.getStatusLine().getStatusCode();

                    Log.d("respuesta", String.valueOf(responseCode));
                    switch (responseCode) {
                        case 200:
                            HttpEntity entity = response.getEntity();
                            if (entity != null) {
                                String responseBody = EntityUtils.toString(entity);
                                Log.d("response:", responseBody);
                                if ("Done".equals(responseBody)) {


                                    File myDir =  new File(MEDIA_PATH_REGISTRO);
                                    myDir.mkdir();

                                    Intent intent = new Intent(getApplicationContext(), CMP.class);
                                    startActivity(intent);


                                    System.exit(0);
                                    onDestroy();
                                    finish();
                                } else {
                                    if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
                                        Toast.makeText(Registro.this,
                                                "Número de registro no encontrado.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(Registro.this,
                                                "User is not found", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            }
                            break;
                    }


                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                } catch (IOException e) {
                    // TODO Auto-generated catch block

                }


            }



        }
    }
}

