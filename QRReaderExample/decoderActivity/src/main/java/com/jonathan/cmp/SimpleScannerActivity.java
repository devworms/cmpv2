package com.jonathan.cmp;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 03/02/15.
 */
public class SimpleScannerActivity extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();

    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Album id
    String ID;
    String tabla;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    private static String LENGUAJE = "";
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(SimpleScannerActivity.this));
        conta();

        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(SimpleScannerActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
        // calling background thread
       // new LoadBanners().execute();



        LENGUAJE = Locale.getDefault().getDisplayLanguage();
        Log.d("Lenguaje:", LENGUAJE);


        TextView Titulo = (TextView) findViewById(R.id.txtTitulo);
        TextView Descripcion = (TextView) findViewById(R.id.txtDescrip);
        TextView DescripcionTwo = (TextView) findViewById(R.id.txtDescripTwo);
        Button Activar = (Button) findViewById(R.id.btnSubmit);

        Titulo.setText("SCANNER");

        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
            Descripcion.setText("Escanea el código QR localizado en el gafete de los asistentes para agregar el contacto en tu teléfono.");
            DescripcionTwo.setText("Los contactos se guardan en su agenda con la nota Congreso Mexicano del Petróleo para que los puedas encontrar fácilmente.");
            Activar.setText("ACTIVAR SCANNER");
        } else {
            Descripcion.setText("Scan the QR code from the  attendees badges to save the contact in your smartphone.");
            DescripcionTwo.setText("The contacts are stored with the note Congreso Mexicano del Petroleo so you can easily find them. ");
            Activar.setText("ACTIVATE SCANNER");
        }

        findViewById(R.id.btnSubmit).setOnClickListener(new ActionButton1());




    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    class ActionButton1 implements View.OnClickListener {
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), DecoderActivity.class);
            startActivity(intent);
        }
    }


    @Override
    public boolean onKeyUp( int keyCode, KeyEvent event )
    {
        if( keyCode == KeyEvent.KEYCODE_BACK )
        {
            onDestroy();
        }
        return super.onKeyUp( keyCode, event );
    }

}
