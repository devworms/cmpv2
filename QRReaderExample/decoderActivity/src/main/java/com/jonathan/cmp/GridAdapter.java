package com.jonathan.cmp;

/**
 * Created by lhuu on 02/02/15.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class GridAdapter extends BaseAdapter implements ListAdapter {

    private Context context;
    // Progress Dialog
    private ProgressDialog pDialog;

    public GridAdapter(Context context) {
        super();
        this.context = context;

        GridViewConfig.addImageUrls();
    }


    public int getCount() {
        return GridViewConfig.getResim_list().size();

    }

    public Object getItem(int position) {

        return GridViewConfig.getResim_list().get(position);
    }


    public long getItemId(int position) {



        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if(convertView==null)
        {
            imageView=new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(100,100));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5,5,5,5);
        }else{
            imageView=(ImageView)convertView;

        }
        imageView.setImageDrawable(LoadImageFromURL(GridViewConfig.
                getResim_list().get(position)));
        return imageView;
    }

    private Drawable LoadImageFromURL(String url)

    {
        try
        {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src");
            return d;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
