package com.jonathan.cmp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salvador on 27/04/2016.
 */
public class ProgramSociaDepor extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();



    ArrayList<String> NotificationList = new ArrayList<String>();
    ArrayList<Integer> NotificationIDList = new ArrayList<Integer>();
    private static final String MEDIA_PATH_NOTIFI = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/Notificaciones/";
    private static final String URL_NOTIFI = "http://app-ecodsa.com.mx/APP/alerta.php";
    private static final String TAG_ID_NOTIFI = "id";
    private static final String TAG_NAME_NOTIFI = "alerta";

    String titulotex;
    Drawable dra;
    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();
    CargaBanner cb= new CargaBanner();
    // Progress Dialog
    private ProgressDialog pDialog;

    String TITULO;
    String tabla;
    String FECHA;
    String TIPO;

    String[] eventos;




    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soci_depo);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(ProgramSociaDepor.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        cd = new ConnectionDetector(getApplicationContext());

        // Check for internet connection
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(ProgramSociaDepor.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }



        // new LoadBanners().execute();

        // Get album id, song id
        Intent i = getIntent();
        TITULO = i.getStringExtra("TITULO");
        tabla = i.getStringExtra("tabla");
        FECHA = i.getStringExtra("fecha");
        TIPO = i.getStringExtra("tipo");

        Log.d("TITULO:", TITULO);
        Log.d("tabla:", tabla);

        if("acompanante".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_acompanantes);
            titulotex="PROGRAMA ACOMPAÑANTES";
            eventos = new String[]{"Tour de Arqueología Industrial y Cultural", "Nuestro Orgullo Regional (Grutas de García)", "Saltillo Mágico"};

        }else if("deportivo".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_programasocial);
            titulotex="PROGRAMA SOCIAL & DEPORTIVO";
            eventos = new String[]{"Carrera 5Km", "Evento de Clausura", "Ceremonia de Inaguracion", "Torneo de Golf"};

        }
        if("SOCIAL & SPORT PROGRAM".equals(TITULO)){
            titulotex=TITULO;
        }else if("COMPANION PROGRAM".equals(TITULO)){
            titulotex=TITULO;
        }

        ImageView imgTitu= (ImageView)findViewById(R.id.imvTitu);
        imgTitu.setImageDrawable(dra);
        final TextView Titulo = (TextView) findViewById(R.id.txtTitulo);

        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        Titulo.setTypeface(font);
        Titulo.setText(TITULO);

        // Hashmap for ListView


        // Loading Albums JSON in Background Thread

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(ProgramSociaDepor.this, R.layout.formato_lista_simple, eventos);


        // get listview
        ListView list = (ListView)findViewById(R.id.listview);

        list.setAdapter(adaptador);

        /**
         * Listview item click listener
         * TrackListActivity will be lauched by passing album id
         * */
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int arg2,
                                    long arg3) {
                // on selecting a single album
                // TrackListActivity will be launched to show tracks inside the album
                Log.d("numero:","dato"+ arg2);

                   Intent i = new Intent(getApplicationContext(), DepoAcompActivity.class);
                    // send album id to tracklist activity to get list of songs under that album
                    String album_id = eventos[arg2];
                    i.putExtra("numEvem", album_id);
                    i.putExtra("tabla",tabla);
                    i.putExtra("TITULO",TITULO);
                    startActivity(i);

            }
        });
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };


}
