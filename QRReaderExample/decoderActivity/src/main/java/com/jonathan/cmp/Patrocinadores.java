package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by mgarza on 07/04/2016.
 */
public class Patrocinadores extends Activity implements OnClickListener{

    String TITULO;
    String tabla;
    String Idioma;
    Button btnPlatino ;
    Button btnOro ;
    Button btnPlata;
    Button btnBronce;
    CargaBanner cb= new CargaBanner();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patrocinadores_a);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(Patrocinadores.this));
        conta();
        TextView Titulo = (TextView) findViewById(R.id.txtTitulo);
        Intent i = getIntent();
        TITULO = i.getStringExtra("TITULO");
        tabla = i.getStringExtra("tabla");
        Idioma =i.getStringExtra("idio");
        Titulo.setText(TITULO);
//        findViewById(R.id.btnPlatino).setOnClickListener(new btnPlatino());
//        findViewById(R.id.btnPlatino).setOnClickListener(new btnPlatino());
//        findViewById(R.id.btnPlatino).setOnClickListener(new btnPlatino());
//        findViewById(R.id.btnPlatino).setOnClickListener(new btnPlatino());
        btnPlatino = (Button) findViewById(R.id.btnPlatino);
        btnPlatino.setOnClickListener(this); // calling onClick() method

        btnOro = (Button) findViewById(R.id.btnOro);
        btnOro.setOnClickListener(this);// calling onClick() method

        btnPlata= (Button) findViewById(R.id.btnPlata);
        btnPlata.setOnClickListener(this);// calling onClick() method

        btnBronce= (Button) findViewById(R.id.btnBronce);
        btnBronce.setOnClickListener(this);// calling onClick() method
        if("Ingles".equals(Idioma)){
            btnPlatino.setText("PLATINUM");
            btnOro.setText("GOLD");
            btnPlata.setText("SILVER");
            btnBronce.setText("BRONZE");


        }
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    @Override
    public void onClick(View v) {
        String lStrPatType ="";
        switch (v.getId()) {

            case R.id.btnPlatino:
                lStrPatType=btnPlatino.getText().toString();
                break;
            case R.id.btnOro:
                lStrPatType=btnOro.getText().toString();
                break;
            case R.id.btnPlata:
                lStrPatType=btnPlata.getText().toString();
                break;
            case R.id.btnBronce:
                lStrPatType=btnBronce.getText().toString();
                break;
        }
        Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
        intent.putExtra("TITULO", lStrPatType);
        intent.putExtra("tabla",  "patrocinador");
        intent.putExtra("fecha", "nivel");
        intent.putExtra("tipo","nivel");
        startActivity(intent);
    }
/*    class btnPlatino implements OnClickListener {
        public void onClick(View view){
            Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
            intent.putExtra("TITULO", "PLATINO");
            intent.putExtra("tabla",  "patrocinador");
            intent.putExtra("fecha", "nivel");
            intent.putExtra("tipo","nivel");
            startActivity(intent);
        }
    }
*/
}