package com.jonathan.cmp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 02/02/15.
 */
public class EncuestasDetail extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    HttpClient httpclient ;
    HttpPost httppost ;

    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    // Album id
    String ID;
    String tabla;

    String id, name, preg1, preg2, preg3, preg4, txtCalific;
    private RatingBar ratingBar;
    private String txtRatingValue = "5";

    private RadioGroup radioPreg1Group;
    private RadioGroup radioPreg2Group;
    private RadioGroup radioPreg3Group;
    private RadioGroup radioPreg4Group;
    private RadioButton radioPreg1;
    private RadioButton radioPreg2;
    private RadioButton radioPreg3;
    private RadioButton radioPreg4;

    private ArrayList<NameValuePair> nameValuePairs;

    // single song JSON url
    // GET parameters album, song

    String upLoadServerUri = null;

    private static final String URL_SONG = "http://app-ecodsa.com.mx/APP/descripcion.php";

    // ALL JSON node names
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "evento";
    private static final String TAG_PREG1 = "p1e";
    private static final String TAG_PREG2 = "p2e";
    private static final String TAG_PREG3 = "p3e";
    private static final String TAG_PREG4 = "p4e";
    private static final String TAG_TXTCA = "califice";
    CargaBanner cb= new CargaBanner();
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuestas);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(EncuestasDetail.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(EncuestasDetail.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
       // new LoadBanners().execute();

        // Get album id, song id
        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        tabla = i.getStringExtra("tabla");
        TextView titulo=(TextView) findViewById(R.id.txtTitulo);
        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        titulo.setTypeface(font);
        // calling background thread
        findViewById(R.id.btnCalificar).setOnClickListener(new ActionButton2());
       // new LoadSingleTrack().execute();
    }

    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    /**
     * Background Async Task to get single song information
     * */

    class ActionButton2 implements View.OnClickListener {
        public void onClick(View v) {
            radioPreg1Group = (RadioGroup) findViewById(R.id.Resp1);
            radioPreg2Group = (RadioGroup) findViewById(R.id.Resp2);
            radioPreg3Group = (RadioGroup) findViewById(R.id.Resp3);
            // get selected radio button from radioGroup
            int selectedId1 = radioPreg1Group.getCheckedRadioButtonId();
            int selectedId2 = radioPreg2Group.getCheckedRadioButtonId();
            int selectedId3 = radioPreg3Group.getCheckedRadioButtonId();


            // find the radiobutton by returned id
            radioPreg1 = (RadioButton) findViewById(selectedId1);
            radioPreg2 = (RadioButton) findViewById(selectedId2);
            radioPreg3 = (RadioButton) findViewById(selectedId3);

            

                String resultados = radioPreg1.getText() + "/" + radioPreg2.getText() + "/" + radioPreg3.getText() + "/"  + "";


            httpclient = new DefaultHttpClient();
            httppost = new HttpPost("http://app-ecodsa.com.mx/APP/calificar.php");

            try{ // make sure the url is correct.
                //add your data
                nameValuePairs = new ArrayList<NameValuePair>(2);
                // Always use the same variable name for posting i.e the android side variable name and php side variable name should be similar,
                nameValuePairs.add(new BasicNameValuePair("evento",name));  // $Edittext_value = $_POST['Edittext_value'];
                nameValuePairs.add(new BasicNameValuePair("idevento",id));
                nameValuePairs.add(new BasicNameValuePair("resp1", radioPreg1.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("resp2", radioPreg2.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("resp3", radioPreg3.getText().toString()));



                nameValuePairs.add(new BasicNameValuePair("califica", ""));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                int responseCode = response.getStatusLine().getStatusCode();
                switch(responseCode)
                {
                    case 200:
                        HttpEntity entity = response.getEntity();
                        if(entity != null)
                        {
                            String responseBody = EntityUtils.toString(entity);
                            Log.d("response:", responseBody);
                            if("Done".equals(responseBody)){
                                Toast.makeText(EncuestasDetail.this,
                                        "Tu calificación fue enviada correctamente.", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(EncuestasDetail.this,
                                        "Verifica tu conexión a internet e intentalo mas de nuevo.", Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                }


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }


    class LoadBanners extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("path", "publicidad"));
            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_BANNERS, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", "> " + json);

            try {
                albums = new JSONArray(json);

                if (albums != null) {
                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);


                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        String tabletSize = getResources().getString(R.string.screen_type);
                        Log.d("pantalla", tabletSize);
                        if ("phone".equals(tabletSize)) {
                            // do something
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_BANNER);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app-ecodsa.com.mx/publicidadc/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        } else {
                            // do something else
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_TABLET);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER_TABLET, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app-ecodsa.com.mx/publicidadt/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        }

                        //ImageView imgView = (ImageView)findViewById(R.id.Banner);

                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {


                    final String[] simpleArray = BannerList.toArray(new String[BannerList.size()]);
                    final String[] LinksimpleArray = LinkBannerList.toArray(new String[LinkBannerList.size()]);
                    imageButton = (ImageButton) findViewById(R.id.Banner);


                    final Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        int i=0;
                        public void run() {


                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int width = size.x;

                            String tabletSize = getResources().getString(R.string.screen_type);
                            Log.d("pantalla", tabletSize);
                            if ("phone".equals(tabletSize)) {
                                // do something
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/1080 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                // do something else
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/2048 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            }

                            i++;
                            if(i>simpleArray.length-1)
                            {
                                i=0;
                            }
                            handler.postDelayed(this, 5000);  //for interval...
                        }
                    };
                    handler.postDelayed(runnable, 1000); //for initial delay..
                }
            });

        }



        private void downloadImagesToSdCard(String downloadUrl,String imageName)
        {
            try{
                URL url = new URL(downloadUrl); //you can write here any link

                File myDir =  new File(MEDIA_PATH_BANNER);
                //Something like ("/sdcard/file.mp3")


                if(!myDir.exists()){
                    myDir.mkdir();
                    Log.v("", "inside mkdir");

                }

                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = imageName;
                File file = new File(myDir, fname);
                if (file.exists ()) file.delete ();

             /* Open a connection to that URL. */
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection)ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

            /*
             * Define InputStreams to read from the URLConnection.
             */
                // InputStream is = ucon.getInputStream();
            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */

                FileOutputStream fos = new FileOutputStream(file);
                int size = 1024*1024;
                byte[] buf = new byte[size];
                int byteRead;
                while (((byteRead = inputStream.read(buf)) != -1)) {
                    fos.write(buf, 0, byteRead);
                    int bytesDownloaded = byteRead;
                }
            /* Convert the Bytes read to a String. */

                fos.close();

            }catch(IOException io)
            {
                io.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

    }

}
