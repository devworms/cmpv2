package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salvador on 08/04/2016.
 */
public class Transportacion extends Activity {
    CargaBanner cb= new CargaBanner();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transportacion);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);

        imgbbanner.setImageDrawable(cb.cargaimage(Transportacion.this));
        conta();
        findViewById(R.id.btnHoteles).setOnClickListener(new hoteles());
        findViewById(R.id.btnAcompa).setOnClickListener(new acompa());
        findViewById(R.id.btnSociDepo).setOnClickListener(new socidepo());
        findViewById(R.id.btnVueloAero).setOnClickListener(new vueloaero());
    }
    class hoteles implements View.OnClickListener{
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), TransporHoteles.class);
            intent.putExtra("Tipo","1");
            startActivity(intent);
        }
    }
    class acompa implements View.OnClickListener{
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), TransporHoteles.class);
            intent.putExtra("Tipo","2");
            startActivity(intent);
        }
    }
    class socidepo implements View.OnClickListener{
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), TransporHoteles.class);
            intent.putExtra("Tipo","3");
            startActivity(intent);
        }
    }
    class vueloaero implements View.OnClickListener{
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), TransporVuelos.class);

            startActivity(intent);
        }
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
}
