package com.jonathan.cmp;

/**
 * Created by lhuu on 02/02/15.
 */


import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class GridViewConfig {

    private static final String URL_SONG = "http://app.ecodsa.com.mx/APP/patrocinadores.php";
    private static ArrayList<String> resim_list=new ArrayList<String>();

    public static ArrayList<String> getResim_list() {
        return resim_list;


    }

    public static void setResim_list(ArrayList<String> resim_list) {
        GridViewConfig.resim_list = resim_list;
    }
    public static void addImageUrls(){
        JSONParser jsonParser = new JSONParser();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        // post album id, song id as GET parameters
        params.add(new BasicNameValuePair("path", "img"));
        resim_list.clear();
        String json = jsonParser.makeHttpRequest(URL_SONG, "GET",
                params);

        // Check your log cat for JSON reponse
        Log.d("DATA JSON: ", json);

        JSONArray albums = null;
        try {
            albums = new JSONArray(json);
            if(albums != null){
                for (int i = 0; i < albums.length(); i++) {
                    JSONObject c = albums.getJSONObject(i);

                    String name = c.getString("nombre");
                    resim_list.add("http://app.ecodsa.com.mx/galeria/" + name);
                }
            }else{
                Log.d("Data: ", "null");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
