package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salvador on 08/04/2016.
 */
public class WebviewInsHosp extends Activity {
    String url;
    CargaBanner cb= new CargaBanner();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inshos_webview);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(WebviewInsHosp.this));
        conta();
        WebView web =  (WebView) findViewById(R.id.wbInsHos);

        Intent i = getIntent();
        String resp=i.getStringExtra("Tipo");
        String titulo=i.getStringExtra("TITULO");
        TextView titu= (TextView)findViewById(R.id.txtTitulo);
        titu.setText(titulo);
        Log.d("respu",resp);

        web.loadUrl(resp);


    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
}
