package com.jonathan.cmp;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

public class MyService extends Service {

    private Service context;


    public MyService() {
        context = this;

        Log.i("servicio", "start");

    }

    Date fechaEvento;
    Date now1;
   private TimerTask timerTask = new TimerTask()
    {
        public void run()
        {


            Calendar c1 = Calendar.getInstance();

            AdminSQLiteAgenda dbHandler;
            dbHandler = new AdminSQLiteAgenda(context, null, null, 1);
            SQLiteDatabase db = dbHandler.getWritableDatabase();
            Cursor cursor = dbHandler.agendaPush();


            if(cursor.getCount() != 0) {
                do {

                    String fechaCam= cursor.getString(0).replace(" ",",");

                    String[] fechaAgen = fechaCam.split(",");
                    String[] horaMin = cursor.getString(1).split(":");



                    int dia= c1.get(Calendar.DAY_OF_MONTH);
                    if (dia== Integer.parseInt(fechaAgen[1])) {
                        if(c1.get(Calendar.HOUR_OF_DAY)==Integer.parseInt(horaMin[0])-1){
                            Log.i("Servicio","entro");
                            PushProgramada push = new PushProgramada(context);
                            push.notify("Recordatorio", "Tienes un evento agendado");

                            dbHandler.borrarEvent(cursor.getString(2).toString());
                        }

                    }




                } while(cursor.moveToNext());


            }else{
                //context.stopSelf();
            }


        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public IBinder onBind(Intent intent) {


        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 12500);

        return super.onStartCommand(intent, flags, startId);



    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



}
