package com.jonathan.cmp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;

/**
 * Created by lhuu on 03/02/15.
 */
public class EncuestasDetailENG extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app.ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";
    //

    HttpClient httpclient ;
    HttpPost httppost ;

    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    // Album id
    String ID;
    String tabla;

    String id, name, preg1, preg2, preg3, preg4, txtCalific;
    private RatingBar ratingBar;
    private String txtRatingValue;

    private RadioGroup radioPreg1Group;
    private RadioGroup radioPreg2Group;
    private RadioGroup radioPreg3Group;
    private RadioGroup radioPreg4Group;
    private RadioButton radioPreg1;
    private RadioButton radioPreg2;
    private RadioButton radioPreg3;
    private RadioButton radioPreg4;

    private ArrayList<NameValuePair> nameValuePairs;

    // single song JSON url
    // GET parameters album, song

    String upLoadServerUri = null;

    private static final String URL_SONG = "http://app.ecodsa.com.mx/APP/descripcion.php";

    // ALL JSON node names
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "eventoi";
    private static final String TAG_PREG1 = "p1i";
    private static final String TAG_PREG2 = "p2i";
    private static final String TAG_PREG3 = "p3i";
    private static final String TAG_PREG4 = "p4i";
    private static final String TAG_TXTCA = "califici";

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuestas);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
          //  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(EncuestasDetailENG.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
        //new LoadBanners().execute();

        RadioButton malo1 = (RadioButton) findViewById(R.id.Malo1);
        RadioButton malo2 = (RadioButton) findViewById(R.id.Malo2);
        RadioButton malo3 = (RadioButton) findViewById(R.id.Malo3);
        RadioButton malo4 = (RadioButton) findViewById(R.id.Malo4);

        RadioButton regu1 = (RadioButton) findViewById(R.id.Regu1);
        RadioButton regu2 = (RadioButton) findViewById(R.id.Regu2);
        RadioButton regu3 = (RadioButton) findViewById(R.id.Regu3);
        RadioButton regu4 = (RadioButton) findViewById(R.id.Regu4);

        RadioButton buen1 = (RadioButton) findViewById(R.id.Buen1);
        RadioButton buen2 = (RadioButton) findViewById(R.id.Buen2);
        RadioButton buen3 = (RadioButton) findViewById(R.id.Buen3);
        RadioButton buen4 = (RadioButton) findViewById(R.id.Buen4);

        malo1.setText("Bad");
        malo2.setText("Bad");
        malo3.setText("Bad");
        malo4.setText("Bad");

        regu1.setText("Regular");
        regu2.setText("Regular");
        regu3.setText("Regular");
        regu4.setText("Regular");

        buen1.setText("Good");
        buen2.setText("Good");
        buen3.setText("Good");
        buen4.setText("Good");


        // Get album id, song id
        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        tabla = i.getStringExtra("tabla");
        TextView titulo=(TextView) findViewById(R.id.txtTitulo);
        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        titulo.setTypeface(font);
        // calling background thread
        new LoadSingleTrack().execute();
    }


    /**
     * Background Async Task to get single song information
     * */
    class LoadSingleTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EncuestasDetailENG.this);
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting song json and parsing
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            // post album id, song id as GET parameters
            params.add(new BasicNameValuePair("id", ID));
            params.add(new BasicNameValuePair("tabla", tabla));

            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_SONG, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("Single Track JSON: ", json);

            try {
                JSONObject jObj = new JSONObject(json);
                if(jObj != null){
                    id = jObj.getString(TAG_ID);
                    name = jObj.getString(TAG_NAME);
                    preg1 = jObj.getString(TAG_PREG1);
                    preg2 = jObj.getString(TAG_PREG2);
                    preg3 = jObj.getString(TAG_PREG3);
                    preg4 = jObj.getString(TAG_PREG4);
                    txtCalific = jObj.getString(TAG_TXTCA);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting song information
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {


                    ratingBar = (RatingBar) findViewById(R.id.ratingBar);

                    ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                        public void onRatingChanged(RatingBar ratingBar, float rating,
                                                    boolean fromUser) {
                            float d= (float) (rating*2);
                            txtRatingValue = String.valueOf(d);

                        }
                    });

                    radioPreg1Group = (RadioGroup) findViewById(R.id.Resp1);
                    radioPreg2Group = (RadioGroup) findViewById(R.id.Resp2);
                    radioPreg3Group = (RadioGroup) findViewById(R.id.Resp3);
                    radioPreg4Group = (RadioGroup) findViewById(R.id.Resp4);

                    if(preg2.equals("")){
                        radioPreg2Group.setVisibility(View.INVISIBLE);
                    }
                    if(preg3.equals("")){
                        radioPreg3Group.setVisibility(View.INVISIBLE);
                    }
                    if(preg4.equals("")){
                        radioPreg4Group.setVisibility(View.INVISIBLE);
                    }

                    TextView txt_name = (TextView) findViewById(R.id.evento);
                    TextView txt_pregunta_1 = (TextView) findViewById(R.id.txtPreg1);
                    TextView txt_pregunta_2 = (TextView) findViewById(R.id.txtPreg2);
                    TextView txt_pregunta_3 = (TextView) findViewById(R.id.txtPreg3);
                    TextView txt_pregunta_4 = (TextView) findViewById(R.id.txtPreg4);
                    TextView txtCalificacion = (TextView) findViewById(R.id.txtCalifica);

                    // displaying song data in view
                    txt_name.setText(name);
                    txt_pregunta_1.setText(preg1);
                    txt_pregunta_2.setText(preg2);
                    txt_pregunta_3.setText(preg3);
                    txt_pregunta_4.setText(preg4);
                    txtCalificacion.setText(txtCalific);

                    // Change Activity Title with Song title
                    setTitle(name);
                    findViewById(R.id.btnCalificar).setOnClickListener(new ActionButton2());
                }
            });

        }

    }

    class ActionButton2 implements View.OnClickListener {
        public void onClick(View v) {
            // get selected radio button from radioGroup
            int selectedId1 = radioPreg1Group.getCheckedRadioButtonId();
            int selectedId2 = radioPreg2Group.getCheckedRadioButtonId();
            int selectedId3 = radioPreg3Group.getCheckedRadioButtonId();
            int selectedId4 = radioPreg4Group.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            radioPreg1 = (RadioButton) findViewById(selectedId1);
            radioPreg2 = (RadioButton) findViewById(selectedId2);
            radioPreg3 = (RadioButton) findViewById(selectedId3);
            radioPreg4 = (RadioButton) findViewById(selectedId4);


            String resultados = radioPreg1.getText() + "/" + radioPreg2.getText() + "/" + radioPreg3.getText() + "/" + radioPreg4.getText() + "/" + txtRatingValue;


            httpclient = new DefaultHttpClient();
            httppost = new HttpPost("http://app.ecodsa.com.mx/APP/calificar.php");

            try{ // make sure the url is correct.
                //add your data
                nameValuePairs = new ArrayList<NameValuePair>(2);
                // Always use the same variable name for posting i.e the android side variable name and php side variable name should be similar,
                nameValuePairs.add(new BasicNameValuePair("evento",name));  // $Edittext_value = $_POST['Edittext_value'];
                nameValuePairs.add(new BasicNameValuePair("idevento",id));
                nameValuePairs.add(new BasicNameValuePair("resp1", radioPreg1.getText().toString()));
                if(preg2.equals("")){
                    nameValuePairs.add(new BasicNameValuePair("resp2", ""));
                } else {
                    nameValuePairs.add(new BasicNameValuePair("resp2", radioPreg2.getText().toString()));
                }

                if(preg3.equals("")){
                    nameValuePairs.add(new BasicNameValuePair("resp3", ""));
                } else {
                    nameValuePairs.add(new BasicNameValuePair("resp3", radioPreg3.getText().toString()));
                }

                if(preg4.equals("")){
                    nameValuePairs.add(new BasicNameValuePair("resp4", ""));
                } else {
                    nameValuePairs.add(new BasicNameValuePair("resp4", radioPreg4.getText().toString()));
                }
                nameValuePairs.add(new BasicNameValuePair("califica", txtRatingValue));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                int responseCode = response.getStatusLine().getStatusCode();
                switch(responseCode)
                {
                    case 200:
                        HttpEntity entity = response.getEntity();
                        if(entity != null)
                        {
                            String responseBody = EntityUtils.toString(entity);
                            Log.d("response:", responseBody);
                            if("Done".equals(responseBody)){
                                Toast.makeText(EncuestasDetailENG.this,
                                        "Tu calificación fue enviada correctamente.", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(EncuestasDetailENG.this,
                                        "Verifica tu conexión a internet e intentalo mas de nuevo.", Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                }


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }


    class LoadBanners extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("path", "publicidad"));
            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_BANNERS, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", "> " + json);

            try {
                albums = new JSONArray(json);

                if (albums != null) {
                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);


                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        String tabletSize = getResources().getString(R.string.screen_type);
                        Log.d("pantalla", tabletSize);
                        if ("phone".equals(tabletSize)) {
                            // do something
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_BANNER);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app.ecodsa.com.mx/publicidadc/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        } else {
                            // do something else
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_TABLET);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER_TABLET, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app.ecodsa.com.mx/publicidadt/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        }

                        //ImageView imgView = (ImageView)findViewById(R.id.Banner);

                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {


                    final String[] simpleArray = BannerList.toArray(new String[BannerList.size()]);
                    final String[] LinksimpleArray = LinkBannerList.toArray(new String[LinkBannerList.size()]);
                    imageButton = (ImageButton) findViewById(R.id.Banner);


                    final Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        int i=0;
                        public void run() {


                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int width = size.x;

                            String tabletSize = getResources().getString(R.string.screen_type);
                            Log.d("pantalla", tabletSize);
                            if ("phone".equals(tabletSize)) {
                                // do something
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/1080 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                // do something else
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/2048 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            }

                            i++;
                            if(i>simpleArray.length-1)
                            {
                                i=0;
                            }
                            handler.postDelayed(this, 5000);  //for interval...
                        }
                    };
                    handler.postDelayed(runnable, 1000); //for initial delay..
                }
            });

        }



        private void downloadImagesToSdCard(String downloadUrl,String imageName)
        {
            try{
                URL url = new URL(downloadUrl); //you can write here any link

                File myDir =  new File(MEDIA_PATH_BANNER);
                //Something like ("/sdcard/file.mp3")


                if(!myDir.exists()){
                    myDir.mkdir();
                    Log.v("", "inside mkdir");

                }

                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = imageName;
                File file = new File(myDir, fname);
                if (file.exists ()) file.delete ();

             /* Open a connection to that URL. */
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection)ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

            /*
             * Define InputStreams to read from the URLConnection.
             */
                // InputStream is = ucon.getInputStream();
            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */

                FileOutputStream fos = new FileOutputStream(file);
                int size = 1024*1024;
                byte[] buf = new byte[size];
                int byteRead;
                while (((byteRead = inputStream.read(buf)) != -1)) {
                    fos.write(buf, 0, byteRead);
                    int bytesDownloaded = byteRead;
                }
            /* Convert the Bytes read to a String. */

                fos.close();

            }catch(IOException io)
            {
                io.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

    }

}
