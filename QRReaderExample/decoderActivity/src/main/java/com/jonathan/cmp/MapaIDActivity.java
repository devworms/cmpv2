package com.jonathan.cmp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 28/01/15.
 */
public class MapaIDActivity extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    //
    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    // Album id
    String ID;
    String tabla;
    String TITULO;
    Drawable dra;
    WebView myWebView;

    String id, name, de;

    // single song JSON url
    // GET parameters album, song
    private static final String URL_SONG = "http://app-ecodsa.com.mx/APP/descripcion.php";

    // ALL JSON node names
    private static final String TAG_ID = "id";
    private static String TAG_NAME = "ne";
    private static String TAG_DESC = "de";
    private static String LENGUAJE = "";
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapaid);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(MapaIDActivity.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
       // new LoadBanners().execute();

        myWebView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);

        LENGUAJE = Locale.getDefault().getDisplayLanguage();
        Log.d("Lenguaje:", LENGUAJE);

        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        tabla = i.getStringExtra("tabla");
        TITULO= i.getStringExtra("titulo");
        if("MAPAS".equals(TITULO)){
            dra=this.getResources().getDrawable(R.drawable.seccion_mapa);

        }



        ImageView ImageTitu = (ImageView) findViewById(R.id.imvTitu);
        ImageTitu.setImageDrawable(dra);


        TextView Titulo = (TextView) findViewById(R.id.txtTitulo);

        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        Titulo.setTypeface(font);
        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {

            if("patrocinador".equals(tabla) || "expositor".equals(tabla) || "patrocinador_mapa".equals(tabla) || "expositor_mapa".equals(tabla)) {
                TAG_NAME = "nombre";
                TAG_DESC = "de";
            } else {
                TAG_NAME = "ne";
                TAG_DESC = "de";
            }

        } else {

            if("patrocinador".equals(tabla) || "expositor".equals(tabla) || "patrocinador_mapa".equals(tabla) || "expositor_mapa".equals(tabla)) {
                TAG_NAME = "nombre";
                TAG_DESC = "di";
            } else {
                TAG_NAME = "ni";
                TAG_DESC = "di";
            }

        }

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(MapaIDActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }

        // Get album id, song id

        // calling background thread
        new LoadSingleTrack().execute();
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };

    class LoadSingleTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MapaIDActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting song json and parsing
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            // post album id, song id as GET parameters
            params.add(new BasicNameValuePair("id", ID));
            params.add(new BasicNameValuePair("tabla", tabla));

            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_SONG, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("Single Track JSON: ", json);

            try {
                JSONObject jObj = new JSONObject(json);
                if(jObj != null){
                    id = jObj.getString(TAG_ID);
                    name = jObj.getString(TAG_NAME);
                    de = jObj.getString(TAG_DESC);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting song information
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    TextView txt_name = (TextView) findViewById(R.id.txtTitulo);
                    TextView txt_desc = (TextView) findViewById(R.id.txtDescripcion);

                    // displaying song data in view
                    txt_name.setText(name);
                    txt_desc.setText(de);
                    final ProgressDialog pd = ProgressDialog.show(MapaIDActivity.this, "", "Loading...", true);

                    myWebView.setWebViewClient(new WebViewClient() {
                        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                            Toast.makeText(MapaIDActivity.this, description, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onPageStarted(WebView view, String url, Bitmap favicon)
                        {
                            pd.show();
                        }


                        @Override
                        public void onPageFinished(WebView view, String url) {
                            String webUrl = myWebView.getUrl();
                            pd.dismiss();
                        }

                     });


                    if("patrocinador".equals(tabla) || "patrocinador_mapa".equals(tabla)){
                        myWebView.loadUrl("http://docs.google.com/gview?embedded=true&url="+"http://app-ecodsa.com.mx/APP/plano1.pdf");
                    } else if("expositor".equals(tabla) || "expositor_mapa".equals(tabla)){
                        myWebView.loadUrl("http://docs.google.com/gview?embedded=true&url="+"http://app-ecodsa.com.mx/APP/plano1.pdf");
                    } else {
                        myWebView.loadUrl("http://docs.google.com/gview?embedded=true&url="+"http://app-ecodsa.com.mx/APP/plano1.pdf");
                    }
                    // Change Activity Title with Song title
                    setTitle(name);
                }
            });

        }

    }


}
