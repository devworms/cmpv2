package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salvador on 25/03/2016.
 */
public class Agenda extends Activity {
    protected CalendarView calendar;

    CargaBanner cb= new CargaBanner();

    protected ListView assignments;

    protected Calendar c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);
        String LENGUAJE = Locale.getDefault().getDisplayLanguage();
        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {}
        else{
            TextView titulo= (TextView)findViewById(R.id.txtTitulo);
            titulo.setText("DIARY");
        }

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(Agenda.this));
        conta();
        assignments = (ListView) findViewById(R.id.AssignListView);
        consultaBD();



    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    public void consultaBD(){
        AdminSQLiteAgenda dbHandler;
        dbHandler = new AdminSQLiteAgenda(Agenda.this, null, null, 1);
        SQLiteDatabase db = dbHandler.getWritableDatabase();
        Cursor cursor = dbHandler.listarpersonas();
        if (cursor.getColumnName(1)=="") {}else{
            String[] from = new String[]{"fecha","hora", "titulo"};
            int[] to = new int[]{
                    R.id.txtFecha,
                    R.id.txtHora,
                    R.id.txtNom

            };


            final ListView lvlitems = (ListView) findViewById(R.id.AssignListView);
            SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(Agenda.this, R.layout.formato_agenda, cursor, from, to);


            lvlitems.setAdapter(cursorAdapter);
            lvlitems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView parent, View view, int position, long id) {
                    Intent i = new Intent(getApplicationContext(), SingleTrackActivity.class);
                    // send album id to tracklist activity to get list of songs under that album

                    i.putExtra("agenda","t");
                    i.putExtra("pos",position);

                    startActivity(i);

                }
            });
        }
    }
    public void alta(View v) {
        AdminSQLiteOpenHelper dbHandler;
        dbHandler = new AdminSQLiteOpenHelper(this, null, null, 1);
        SQLiteDatabase db = dbHandler.getWritableDatabase();
        //dbHandler.addEvento("Dia normal","Salva","Dia x","Aqui","25 de marzo","11:00");


        Toast.makeText(this, "Se cargaron los datos del artículo",
                Toast.LENGTH_SHORT).show();
    }

}
