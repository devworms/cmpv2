package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salvador on 18/03/2016.
 */
public class Buscar extends Activity {
    EditText inputSearch;
    CargaBanner cb= new CargaBanner();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    String id,nomb,desc,telf,correo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_misexpo);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(Buscar.this));
        conta();
        AdminSQLiteOpenHelper dbHandler;
        dbHandler = new AdminSQLiteOpenHelper(this, null, null, 1);
        SQLiteDatabase db = dbHandler.getWritableDatabase();
        Cursor cursor = dbHandler.listarpersonas();



        String[] from = new String[]{"nombre", "telefono"};
        int[] to = new int[]{
                R.id.miembro_id,
                R.id.miembro_nombre
        };


        final ListView lvlitems = (ListView) findViewById(R.id.lvlitems);
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this, R.layout.formato_fila, cursor, from, to);


        lvlitems.setAdapter(cursorAdapter);
        lvlitems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), PatrocinExposiActivity.class);
                // send album id to tracklist activity to get list of songs under that album

                i.putExtra("busca","t");
                i.putExtra("nom",position);

                startActivity(i);

            }
        });

        AutoCompleteTextView acTextView = (AutoCompleteTextView) findViewById(R.id.autoComplete);
        //acTextView.setAdapter(new SuggestionAdapter(this,acTextView.getText().toString(),tabla ));
        //acTextView.setOnItemClickListener(ListadoActivity.this);
        acTextView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ListView lvlitems = (ListView) findViewById(R.id.lvlitems);

                lvlitems.setAdapter(null);

                new LoadAlbums2().execute();
            }
        });


    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };


    class LoadAlbums2 extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        /**
         * getting Albums JSON
         */
        protected String doInBackground(String... args) {
            // Building Parameters

            AutoCompleteTextView acTextView1 = (AutoCompleteTextView) findViewById(R.id.autoComplete);
            AdminSQLiteOpenHelper dbHandler;
            dbHandler = new AdminSQLiteOpenHelper(Buscar.this, null, null, 1);
            SQLiteDatabase db = dbHandler.getWritableDatabase();
            Cursor cursor = dbHandler.listarpersonasb(acTextView1.getText().toString());
            ListView lvlitems = (ListView) findViewById(R.id.lvlitems);
            String[] from = new String[]{"nombre", "telefono"};
            int[] to = new int[]{
                    R.id.miembro_id,
                    R.id.miembro_nombre
            };
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    AutoCompleteTextView acTextView2 = (AutoCompleteTextView) findViewById(R.id.autoComplete);
                    AdminSQLiteOpenHelper dbHandler;
                    dbHandler = new AdminSQLiteOpenHelper(Buscar.this, null, null, 1);
                    SQLiteDatabase db = dbHandler.getWritableDatabase();
                    Cursor cursor = dbHandler.listarpersonasb(acTextView2.getText().toString());
                    ListView lvlitems = (ListView) findViewById(R.id.lvlitems);
                    String[] from = new String[]{"nombre", "telefono"};
                    int[] to = new int[]{
                            R.id.miembro_id,
                            R.id.miembro_nombre
                    };
                    SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(Buscar.this, R.layout.formato_fila, cursor, from, to);
                    ;

                    lvlitems.setAdapter(cursorAdapter);

                }
            });

        }

    }


}
