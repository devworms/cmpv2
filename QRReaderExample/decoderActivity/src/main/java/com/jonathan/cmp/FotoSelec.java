package com.jonathan.cmp;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by salva on 20/05/2016.
 */

public class FotoSelec extends Activity {
    ImageView imgSelect;
    Bitmap b;
    int numeroImg;
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fotoselect);

      numeroImg= getIntent().getIntExtra("numeroImg",0);
        Log.d("numeroImg","numeroImg "+numeroImg );
        if(getIntent().hasExtra("byteArray")) {
            ImageView previewThumbnail = new ImageView(this);
            b = BitmapFactory.decodeByteArray(
                    getIntent().getByteArrayExtra("byteArray"),0,getIntent().getByteArrayExtra("byteArray").length);
            previewThumbnail.setImageBitmap(b);
        }
        //Bitmap bitmap = getIntent().getParcelableExtra("BitmapImage");
       // Log.d("bitmap", "bitmap"+bitmap.toString());
        imgSelect= (ImageView) findViewById(R.id.imgDialogImage);
        imgSelect.setImageBitmap(b);

        Button btnDescar=(Button)findViewById(R.id.btnDescar);
        btnDescar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                guardarImagen(getApplicationContext(), "imagen"+ numeroImg, b);

            }
        });


    }
    private String guardarImagen (Context context, String nombre, Bitmap imagen){
        ContextWrapper cw = new ContextWrapper(context);
        //File dirImages = cw.getDir("/CMP/banners/cel/",0);
        File myPath = new File(MEDIA_PATH_BANNER, nombre + ".jpg");

        FileOutputStream fos = null;
        Log.d("descarga","si" );
        try{
            Log.d("descarga","empieza" );
            fos = new FileOutputStream(myPath);
            imagen.compress(Bitmap.CompressFormat.JPEG, 10, fos);

            fos.flush();
            Log.d("descarga","empieza" );
        }catch (FileNotFoundException ex){
            ex.printStackTrace();
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return myPath.getAbsolutePath();
    }
}
