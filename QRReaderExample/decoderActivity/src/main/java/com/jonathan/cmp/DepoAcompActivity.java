package com.jonathan.cmp;

/**
 * Created by lhuu on 27/01/15.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class DepoAcompActivity extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();

int cambia=1;
    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    // Album id
    String ID;
    String tabla;
    String titulotex;
    Drawable dra,codVes,codVes2;

    String id, name, fecha, tipo, de, h, lugar, link, img_1, img_2, img_3;

    // single song JSON url
    // GET parameters album, song
    private static final String URL_SONG = "http://app-ecodsa.com.mx/APP/descripcion.php";

    // ALL JSON node names
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "ne";
    private static final String TAG_DESC = "de";
    private static final String TAG_FECH = "fecha";
    private static final String TAG_TIPO = "tipo";
    private static final String TAG_HORA = "h";
    private static final String TAG_LUGA = "lugar";
    private static final String TAG_LINK = "link";
    private static final String TAG_IMG1 = "img_1";
    private static final String TAG_IMG2 = "img_2";
    private static final String TAG_IMG3 = "img_3";
    TextView txt_name,txt_tipoEven,txt_descEven,txt_fechaEva,txt_horaEven,txt_lugarEva,titulo,txt_cupoEven;



    //variables para modo statico
    String namEven,descEven,tipoEven,fechaEven,horaEvn,lugarEven,cupoEven;
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_depacomp);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(DepoAcompActivity.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
          //  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }


        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(DepoAcompActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }


        // Get album id, song id
        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        tabla = i.getStringExtra("tabla");

        namEven=i.getStringExtra("numEvem");

        if("acompanante".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_acompanantes);
            titulotex="PROGRAMA ACOMPAÑANTES";
            if("Tour de Arqueología Industrial y Cultural".equals(namEven)){
                descEven="Traslado de hoteles participantes a Horno 3.";
                tipoEven="Acompañante";
                fechaEven="Jueves 9";
                lugarEven="Salida de lobby de hoteles participantes (de acuerdo a horarios de transportación).";
                horaEvn="8:00 am";
                cupoEven="300 personas";
                codVes=this.getResources().getDrawable(R.drawable.codigovestimenta_sport_acompanantes);
                codVes2=this.getResources().getDrawable(R.drawable.tour_arqueologico);
            }else if("Nuestro Orgullo Regional (Grutas de García)".equals(namEven)){
                descEven="Paseo por varios lugares turisticos.";
                tipoEven="Acompañante";
                fechaEven="Viernes 10";
                lugarEven="Salida de lobby de hoteles";
                horaEvn="09:00 am";
                cupoEven="100 personas";
                codVes=this.getResources().getDrawable(R.drawable.codigovestimenta_sport_acompanantes);
                codVes2=this.getResources().getDrawable(R.drawable.orgullo);
            } else if("Saltillo Mágico".equals(namEven)){
                descEven="Paseo magico por saltillo";
                tipoEven="Acompañante";
                fechaEven="Viernes 10";
                lugarEven="Salida de lobby de hoteles";
                horaEvn="09:00 am";
                cupoEven="200 personas";
                codVes=this.getResources().getDrawable(R.drawable.codigovestimenta_sport_acompanantes);
                codVes2=this.getResources().getDrawable(R.drawable.saltillo);
            }

        }else if("deportivo".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_programasocial);
            titulotex="PROGRAMA SOCIAL & DEPORTIVO";
            if("Carrera 5Km".equals(namEven)){
                descEven="El Boulevard Acero es una pista que mide 3.4 kilómetros de largo y 14 metros de ancho. Ideal para recorrer el parque caminando, trotando, patinando o en bicicleta, en un ambiente seguro y de fácil acceso, el Boulevard es el circuito que rodea las áreas verdes del Parque Fundidora.";
                tipoEven="Deportivo";
                fechaEven="Miercoles 8";
                lugarEven="Pista Boulevard Acero";
                horaEvn="8:00 am";
                codVes=this.getResources().getDrawable(R.drawable.codigovestimenta_sport_acompanantes);
                codVes2=this.getResources().getDrawable(R.drawable.carrera);
            }else if("Evento de Clausura".equals(namEven)){
                descEven="Evento final de congreso";
                tipoEven="Social";
                fechaEven="Viernes 10";
                lugarEven="Sala principal ";
                horaEvn="20:00 hrs";
                codVes=this.getResources().getDrawable(R.drawable.codigovestimenta_formal_socialdeportivo);
                codVes2=this.getResources().getDrawable(R.drawable.clausura);
            } else if("Ceremonia de Inaguracion".equals(namEven)){
                descEven="Ceremonia de apertura del congreso";
                tipoEven="Social";
                fechaEven="Miércoles 8";
                lugarEven="Entrada del recinto";
                horaEvn="18:30 hrs";
                codVes=this.getResources().getDrawable(R.drawable.codigovestimenta_formal_socialdeportivo);
                codVes2=this.getResources().getDrawable(R.drawable.inaguracion);
            }else if("Torneo de Golf".equals(namEven)){
                descEven="Torneo de Golf";
                tipoEven="Deportivo";
                fechaEven="Jueves 9";
                lugarEven="Proximamente";
                horaEvn="Proximamente";
                codVes=this.getResources().getDrawable(R.drawable.codigovestimenta_sport_acompanantes);
                codVes2=this.getResources().getDrawable(R.drawable.golf);
            }

        }
        titulotex = i.getStringExtra("TITULO");

        cambiaImage();

        ImageView ImageTitu = (ImageView) findViewById(R.id.imvTitu);
        ImageTitu.setImageDrawable(dra);
        ImageView ImageCode = (ImageView) findViewById(R.id.imgCod);
        ImageCode.setImageDrawable(codVes);

        titulo=(TextView) findViewById(R.id.txtTitulo);
        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        titulo.setTypeface(font);
        titulo.setText(titulotex);
         txt_name = (TextView) findViewById(R.id.name);
        txt_name.setText(namEven);
         txt_tipoEven=(TextView)findViewById(R.id.tipo);
        txt_tipoEven.setText(tipoEven);
         txt_descEven=(TextView) findViewById(R.id.description);
        txt_descEven.setText(descEven);
         txt_fechaEva=(TextView) findViewById(R.id.fecha);
        txt_fechaEva.setText(fechaEven);
         txt_horaEven=(TextView) findViewById(R.id.hora);
        txt_horaEven.setText(horaEvn);
         txt_lugarEva=(TextView) findViewById(R.id.lugar);
        txt_lugarEva.setText(lugarEven);
        txt_cupoEven=(TextView)findViewById(R.id.cupo);
        txt_cupoEven.setText(cupoEven);
        TextView txt_TagCupo=(TextView)findViewById(R.id.TitCupor);
        if("deportivo".equals(tabla)){
            txt_TagCupo.setVisibility(View.INVISIBLE);
            txt_cupoEven.setVisibility(View.INVISIBLE);
        }

        findViewById(R.id.btnAdd).setOnClickListener(new ActionButton1());


        // calling background thread
       // new LoadSingleTrack().execute();
    }
    class ActionButton1 implements OnClickListener {
        public void onClick(View v) {


            AdminSQLiteAgenda dbHandler;
            dbHandler = new AdminSQLiteAgenda(DepoAcompActivity.this, null, null, 1);
            SQLiteDatabase db = dbHandler.getWritableDatabase();
            String CurrentString = txt_fechaEva.getText().toString();
           // String [] separated = CurrentString.split(",");
            dbHandler.addEvento(txt_name.getText().toString(),"","","",txt_fechaEva.getText().toString(),txt_horaEven.getText().toString(),txt_descEven.getText().toString());


            Toast.makeText(DepoAcompActivity.this, "Se guardo el evento en la agenda",
                    Toast.LENGTH_SHORT).show();
        }
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };

    public void cambiaImage(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask2, 0, 2000);
    }
    TimerTask timerTask2 = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {

                    Log.d("cambia","cam"+ cambia);
                    if (cambia==1){

                        ImageView ImageCode = (ImageView) findViewById(R.id.imgCod);
                        ImageCode.setImageDrawable(codVes);
                        cambia=cambia+1;
                    }
                    else if(cambia==2){

                        ImageView ImageCode = (ImageView) findViewById(R.id.imgCod);
                        ImageCode.setImageDrawable(codVes2);
                        cambia=1;
                    }


                }
            });

        }
    };
    /**
     * Background Async Task to get single song information
     * */
    class LoadSingleTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DepoAcompActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting song json and parsing
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            // post album id, song id as GET parameters
            params.add(new BasicNameValuePair("id", ID));
            params.add(new BasicNameValuePair("tabla", tabla));

            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_SONG, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("Single Track JSON: ", json);

            try {
                JSONObject jObj = new JSONObject(json);
                if(jObj != null){
                    id = jObj.getString(TAG_ID);
                    name = jObj.getString(TAG_NAME);
                    de = jObj.getString(TAG_DESC);
                    fecha = jObj.getString(TAG_FECH);
                    tipo = jObj.getString(TAG_TIPO);
                    h = jObj.getString(TAG_HORA);
                    lugar = jObj.getString(TAG_LUGA);
                    link = jObj.getString(TAG_LINK);
                    img_1 = jObj.getString(TAG_IMG1);
                    img_2 = jObj.getString(TAG_IMG2);
                    img_3 = jObj.getString(TAG_IMG3);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting song information
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    TextView txt_name = (TextView) findViewById(R.id.name);
                    TextView txt_desc = (TextView) findViewById(R.id.description);
                    TextView txt_fech = (TextView) findViewById(R.id.fecha);
                    TextView txt_tipo = (TextView) findViewById(R.id.tipo);
                    TextView txt_hora = (TextView) findViewById(R.id.hora);
                    TextView txt_luga = (TextView) findViewById(R.id.lugar);

                    // displaying song data in view
                    txt_name.setText(name);
                    txt_desc.setText(de);
                    txt_tipo.setText(tipo);
                    txt_fech.setText(fecha);
                    txt_hora.setText(h);
                    txt_luga.setText(lugar);
                    findViewById(R.id.btnAdd).setOnClickListener(new ActionButtonAgre());

                    if("".equals(link)){

                    } else {
                        findViewById(R.id.btnMapa).setOnClickListener(new ActionButtonMapa());
                    }
                    // Change Activity Title with Song title
                    setTitle(name);


                           }
            });

        }


        class ActionButtonMapa implements OnClickListener {
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(link));
                startActivity(i);
            }
        }
        class ActionButtonAgre implements OnClickListener {
            public void onClick(View v) {
                TextView txtTitulo=(TextView) findViewById(R.id.name);
                TextView txtDesc=(TextView) findViewById(R.id.description);
                TextView txtFecha=(TextView) findViewById(R.id.fecha);
                TextView txtHora=(TextView) findViewById(R.id.hora);
                TextView txtTipo=(TextView) findViewById(R.id.tipo);
                TextView txtLugar=(TextView) findViewById(R.id.lugar);

                AdminSQLiteAgenda dbHandler;
                dbHandler = new AdminSQLiteAgenda(DepoAcompActivity.this, null, null, 1);
                SQLiteDatabase db = dbHandler.getWritableDatabase();
                dbHandler.addEvento(txtTitulo.getText().toString(),txtTipo.getText().toString(),txtDesc.getText().toString(),txtLugar.getText().toString(),txtFecha.getText().toString(),txtHora.getText().toString(),txtDesc.getText().toString());


                Toast.makeText(DepoAcompActivity.this, "Se guardo el evento en la agenda",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }


}

