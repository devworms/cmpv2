package com.jonathan.cmp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 31/01/15.
 */


public class MenuPhoto extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;
    //
    private static String LENGUAJE = "";

    // Progress Dialog
    private ProgressDialog pDialog;
    CargaBanner cb= new CargaBanner();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_fotos);


        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(MenuPhoto.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        //new LoadBanners().execute();

        Button BTNIMG2 = (Button) findViewById(R.id.btnGaleria);
//        findViewById(R.id.btnActivarCamara).setOnClickListener(new ActionButton1());
        findViewById(R.id.btnGaleria).setOnClickListener(new ActionButton2());


        ImageView ImageTitu = (ImageView) findViewById(R.id.imvTitu);
        ImageTitu.setImageDrawable(this.getResources().getDrawable(R.drawable.seccion_fotos));
        TextView Titulo = (TextView) findViewById(R.id.txtTitulo);

        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        Titulo.setTypeface(font);
        TextView Descripcion = (TextView) findViewById(R.id.txtDescripcion);

        LENGUAJE = Locale.getDefault().getDisplayLanguage();
        Log.d("Lenguaje:", LENGUAJE);

        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {

            BTNIMG2.setText("GALERÍA");
            Titulo.setText("FOTOS");
            Descripcion.setText("Toma fotografías, compártelas en tus redes sociales y accede a la galería oficial del CMP.");
        } else {

            BTNIMG2.setText("GALLERY");
            Titulo.setText("PHOTOS");
            Descripcion.setText("Take pictures, share them on your social networks and view the official gallery of the CMP.");
        }

    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    class ActionButton1 implements View.OnClickListener {
        public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SharePictureActivity.class);
                startActivity(intent);
        }
    }

    class ActionButton2 implements View.OnClickListener {
        public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GridViewActivity.class);
                startActivity(intent);
        }
    }


}
