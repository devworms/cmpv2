package com.jonathan.cmp;

/**
 * Created by lhuu on 27/01/15.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class SingleTrackActivity extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    //new LoadBanners().execute();
    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    // Album id
    String ID;
    String tabla;
    String agenda;
    int pos;

    String id, name, fecha, hora, de, date;
    String[] separated= new String[2];
    // single song JSON url
    // GET parameters album, song
    private static final String URL_SONG = "http://app-ecodsa.com.mx/APP/descripcion.php";

    // ALL JSON node names
    String TAG_ID = "id";
    String TAG_NAME = "ne";
    String TAG_DESC = "de";
    String TAG_FECH = "fecha";
    String TAG_HORA = "h";
    String TAG_DATE = "date";
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_track);

        String tabletSize = getResources().getString(R.string.screen_type);


        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(SingleTrackActivity.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(SingleTrackActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
       // new LoadBanners().execute();

        // Get album id, song id
        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        tabla = i.getStringExtra("tabla");
        agenda = i.getStringExtra("agenda");
        pos=i.getIntExtra("pos",0);
        String LENGUAJE = Locale.getDefault().getDisplayLanguage();
        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {

        }else{
             TAG_ID = "id";
             TAG_NAME = "ni";
             TAG_DESC = "di";
             TAG_FECH = "fecha";
             TAG_HORA = "h";
             TAG_DATE = "date";

        }
        if(agenda!=null){
            TextView txt_name = (TextView) findViewById(R.id.name);
            TextView txt_fecha = (TextView) findViewById(R.id.date);
            TextView txt_hora = (TextView) findViewById(R.id.clock);
            TextView txt_desc = (TextView) findViewById(R.id.description);
            AdminSQLiteAgenda dbHandler;
            dbHandler = new AdminSQLiteAgenda(SingleTrackActivity.this, null, null, 1);
            SQLiteDatabase db = dbHandler.getWritableDatabase();
            Cursor cursor = dbHandler.listarpersonasid(pos+1);
            txt_name.setText(cursor.getString(1));
            txt_fecha.setText(cursor.getString(5));
            txt_hora.setText(cursor.getString(6));
            txt_desc.setText(cursor.getString(7));
            Button btnAgreg=(Button)findViewById(R.id.btnAgregar);
            btnAgreg.setVisibility(View.INVISIBLE);
        }else {
            // calling background thread

            new LoadSingleTrack().execute();
        }
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    class ActionButton1 implements OnClickListener {
        public void onClick(View v) {
            TextView txtTitulo=(TextView) findViewById(R.id.name);
            TextView txtDesc=(TextView) findViewById(R.id.description);
            TextView txtFecha=(TextView) findViewById(R.id.date);
            TextView txtHora=(TextView) findViewById(R.id.clock);
            String CurrentString = txtFecha.getText().toString();
            //separated = CurrentString.split(",");

            AdminSQLiteAgenda dbHandler;
            dbHandler = new AdminSQLiteAgenda(SingleTrackActivity.this, null, null, 1);
            SQLiteDatabase db = dbHandler.getWritableDatabase();
            try {
                dbHandler.addEvento(txtTitulo.getText().toString(),"", "","",txtFecha.getText().toString(),txtHora.getText().toString(),txtDesc.getText().toString());


            }catch (Exception e){
              //  dbHandler.addEvento(txtTitulo.getText().toString(),"", separated[0],"",txtFecha.getText().toString(),txtHora.getText().toString(),txtDesc.getText().toString());

            }


            Toast.makeText(SingleTrackActivity.this, "Se guardo el evento en la agenda",
                    Toast.LENGTH_SHORT).show();
        }
    }

    class ActionButton11 implements OnClickListener {
        public void onClick(View v) {
            String dateObtain = "";
            if("".equals(hora)){
                dateObtain = date + " 09:00";
            } else {
                dateObtain = date + " " + hora;
            }
            Log.d("fecha", dateObtain);
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            java.util.Date d1 = null;
            Calendar tdy1;

            try {
                d1 = fmt.parse(dateObtain);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            tdy1 = Calendar.getInstance();
            System.out.println("tdy mnthhhhhhhhhhh " + tdy1);
            tdy1.setTime(d1);

            TimeZone timeZone2 = TimeZone.getTimeZone("America/Mexico_City");
            System.out.println("Month of date1= " + tdy1);

            Calendar cal = Calendar.getInstance();
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra("beginTime", tdy1.getTimeInMillis());
            intent.putExtra("allDay", false);
            intent.putExtra("endTime", tdy1.getTimeInMillis()+60*30*1000);
            intent.putExtra("title", name);
            intent.putExtra("description", de);
            intent.putExtra("TimeZone", timeZone2);
            intent.putExtra("eventLocation", "Congreso Mexicano del Petróleo");
            startActivity(intent);
        }
    }


    class ActionButton2 implements OnClickListener {
        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(), MapaIDActivity.class);
            // send album id to tracklist activity to get list of songs under that album
            i.putExtra("ID", ID);
            i.putExtra("tabla", tabla);
            startActivity(i);
        }
    }

    /**
     * Background Async Task to get single song information
     * */
    class LoadSingleTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SingleTrackActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting song json and parsing
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            // post album id, song id as GET parameters
            params.add(new BasicNameValuePair("id", ID));
            params.add(new BasicNameValuePair("tabla", tabla));

            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_SONG, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("Single Track JSON: ", json);

            try {
                JSONObject jObj = new JSONObject(json);
                if(jObj != null){
                    id = jObj.getString(TAG_ID);
                    name = jObj.getString(TAG_NAME);
                    fecha = jObj.getString(TAG_FECH);
                    hora = jObj.getString(TAG_HORA);
                    de = jObj.getString(TAG_DESC);
                    date = jObj.getString(TAG_DATE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting song information
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    String LENGUAJE = Locale.getDefault().getDisplayLanguage();
                    if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {

                        TextView txt_name = (TextView) findViewById(R.id.name);
                        TextView txt_fecha = (TextView) findViewById(R.id.date);
                        TextView txt_hora = (TextView) findViewById(R.id.clock);
                        TextView txt_desc = (TextView) findViewById(R.id.description);

                        // displaying song data in view

                        txt_name.setText(name);
                        txt_fecha.setText(fecha);
                        txt_hora.setText(hora);
                        txt_desc.setText(de);

                        // Change Activity Title with Song title
                        setTitle(name);

                        findViewById(R.id.btnAgregar).setOnClickListener(new ActionButton1());
                        findViewById(R.id.btnLocalizar).setOnClickListener(new ActionButton2());

                    }else{

                        TextView tit_fech = (TextView) findViewById(R.id.TitFecha);
                        TextView tit_hora = (TextView) findViewById(R.id.TitHora);
                        TextView tit_desc = (TextView) findViewById(R.id.TitDesc);

                        Button agregar = (Button) findViewById(R.id.btnAgregar);
                        Button localizar = (Button) findViewById(R.id.btnLocalizar);

                        TextView txt_name = (TextView) findViewById(R.id.name);
                        TextView txt_fecha = (TextView) findViewById(R.id.date);
                        TextView txt_hora = (TextView) findViewById(R.id.clock);
                        TextView txt_desc = (TextView) findViewById(R.id.description);



                        tit_fech.setText("Date");
                        tit_hora.setText("Hour");
                        tit_desc.setText("Description");
                        agregar.setText("ADD TO MY DIARY");
                        localizar.setText("FIND ON MAP");

                        // displaying song data in view
                        txt_name.setText(name);
                        txt_fecha.setText(fecha);
                        txt_hora.setText(hora);
                        txt_desc.setText(de);

                        // Change Activity Title with Song title
                        setTitle(name);

                        findViewById(R.id.btnAgregar).setOnClickListener(new ActionButton1());
                        findViewById(R.id.btnLocalizar).setOnClickListener(new ActionButton2());

                    }
                }
            });

        }

    }

    class LoadBanners extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("path", "publicidad"));
            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_BANNERS, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", "> " + json);

            try {
                albums = new JSONArray(json);

                if (albums != null) {
                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);


                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        String tabletSize = getResources().getString(R.string.screen_type);
                        Log.d("pantalla", tabletSize);
                        if ("phone".equals(tabletSize)) {
                            // do something
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_BANNER);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app-ecodsa.com.mx/publicidadc/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        } else {
                            // do something else
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_TABLET);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER_TABLET, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app-ecodsa.com.mx/publicidadt/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        }

                        //ImageView imgView = (ImageView)findViewById(R.id.Banner);

                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {


                    final String[] simpleArray = BannerList.toArray(new String[BannerList.size()]);
                    final String[] LinksimpleArray = LinkBannerList.toArray(new String[LinkBannerList.size()]);
                    imageButton = (ImageButton) findViewById(R.id.Banner);


                    final Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        int i=0;
                        public void run() {


                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int width = size.x;

                            String tabletSize = getResources().getString(R.string.screen_type);
                            Log.d("pantalla", tabletSize);
                            if ("phone".equals(tabletSize)) {
                                // do something
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/1080 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                // do something else
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/2048 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            }

                            i++;
                            if(i>simpleArray.length-1)
                            {
                                i=0;
                            }
                            handler.postDelayed(this, 5000);  //for interval...
                        }
                    };
                    handler.postDelayed(runnable, 1000); //for initial delay..
                }
            });

        }



        private void downloadImagesToSdCard(String downloadUrl,String imageName)
        {
            try{
                URL url = new URL(downloadUrl); //you can write here any link

                File myDir =  new File(MEDIA_PATH_BANNER);
                //Something like ("/sdcard/file.mp3")


                if(!myDir.exists()){
                    myDir.mkdir();
                    Log.v("", "inside mkdir");

                }

                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = imageName;
                File file = new File(myDir, fname);
                if (file.exists ()) file.delete ();

             /* Open a connection to that URL. */
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection)ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

            /*
             * Define InputStreams to read from the URLConnection.
             */
                // InputStream is = ucon.getInputStream();
            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */

                FileOutputStream fos = new FileOutputStream(file);
                int size = 1024*1024;
                byte[] buf = new byte[size];
                int byteRead;
                while (((byteRead = inputStream.read(buf)) != -1)) {
                    fos.write(buf, 0, byteRead);
                    int bytesDownloaded = byteRead;
                }
            /* Convert the Bytes read to a String. */

                fos.close();

            }catch(IOException io)
            {
                io.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

    }


}

