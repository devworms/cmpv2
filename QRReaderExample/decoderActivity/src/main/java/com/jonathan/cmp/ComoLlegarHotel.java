package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salva on 11/05/2016.
 */
public class ComoLlegarHotel extends Activity {
    CargaBanner cb= new CargaBanner();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_como_llegar_hotel);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(ComoLlegarHotel.this));
        conta();
        findViewById(R.id.btnHotel1).setOnClickListener(new ActionButton1());
        findViewById(R.id.btnHotel2).setOnClickListener(new ActionButton2());
        findViewById(R.id.btnHotel3).setOnClickListener(new ActionButton3());
        findViewById(R.id.btnHotel4).setOnClickListener(new ActionButton4());
        findViewById(R.id.btnHotel5).setOnClickListener(new ActionButton5());
        findViewById(R.id.btnHotel6).setOnClickListener(new ActionButton6());
        findViewById(R.id.btnHotel7).setOnClickListener(new ActionButton7());
        findViewById(R.id.btnHotel8).setOnClickListener(new ActionButton8());
        findViewById(R.id.btnHotel9).setOnClickListener(new ActionButton9());
        findViewById(R.id.btnHotel10).setOnClickListener(new ActionButton10());
        findViewById(R.id.btnHotel11).setOnClickListener(new ActionButton11());





    }

    class ActionButton1 implements View.OnClickListener {
        public void onClick(View v) {

            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Fiesta+Americana/@25.66678,-100.3138317,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0xf3edde47b0346ac2!8m2!3d25.66678!4d-100.311643"));

            startActivity(ir);
        }
    }
    class ActionButton2 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Gamma+de+Fiesta+Americana+Monterrey+Gran+Hotel+Ancira/@25.6660127,-100.3146293,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0xed468a61529236f3!8m2!3d25.6660127!4d-100.3124406"));

            startActivity(ir);

        }
    }
    class ActionButton3 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Crowne+Plaza+Monterrey/@25.664913,-100.3162527,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0xb2eea6d1a9dd212f!8m2!3d25.664913!4d-100.314064"));
            startActivity(ir);
        }
    }
    class ActionButton4 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Fiesta+Americana+Pabellon+M/@25.6656224,-100.3191232,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x978633c13afd34bb!8m2!3d25.6656224!4d-100.3169345"));
            startActivity(ir);
        }
    }
    class ActionButton5 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Antaris+Galer%C3%ADas/@25.6684967,-100.3370294,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0xe29ce12fefc9952e!8m2!3d25.6684967!4d-100.3348407"));
            startActivity(ir);
        }
    }
    class ActionButton6 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse(""));
            startActivity(ir);
        }
    }
    class ActionButton7 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Antaris+Galer%C3%ADas/@25.6684967,-100.3370294,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0xe29ce12fefc9952e!8m2!3d25.6684967!4d-100.3348407"));
            startActivity(ir);
        }
    }
    class ActionButton8 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Antarisuite+Cintermex/@25.6775063,-100.2924542,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x63e61567d1566c72!8m2!3d25.6775063!4d-100.2902655"));
            startActivity(ir);
        }
    }
    class ActionButton9 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Fiesta+Inn+Monterrey+Fundidora/@25.684382,-100.2715887,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x92c787699d69a9d7!8m2!3d25.684382!4d-100.2694"));
            startActivity(ir);
        }
    }
    class ActionButton10 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse(""));
            startActivity(ir);
        }
    }
    class ActionButton11 implements View.OnClickListener {
        public void onClick(View v) {
            Intent ir= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Hotel+Presidente+InterContinental+Monterrey/@25.653441,-100.3413697,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x47b72589d6204e75!8m2!3d25.653441!4d-100.339181"));
            startActivity(ir);
        }
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
}
