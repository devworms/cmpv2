package com.jonathan.cmp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.Toast;

import com.jonathan.cmp.mobile.AWSMobileClient;
import com.jonathan.cmp.mobile.user.IdentityManager;

import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class CMP extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private static final String MEDIA_PATH_REGISTRO = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/Regis/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);

    /** Class name for log messages. */
    private final static String LOG_TAG = CMP.class.getSimpleName();

    private IdentityManager identityManager;

    // albums JSONArray
    JSONArray albums = null;

    private static ImageView image;
    private static ImageButton imageButton;

    // albums JSON url
    private static final String URL_ALBUMS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";

    // ALL JSON node names
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_LINK = "nombre";
    // Connection detector
    ConnectionDetector cd;
    String nombre;
    String idioma;

    private Timer timer;

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();

    // Progress Dialog

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();
    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    private static String TAG_PROGRAMA = "CONFERENCES";
    private static String TAG_PATROCIN = "SPONSORS";
    private static String TAG_EXPOSITO = "EXHIBITORS";
    private static String TAG_PROSOYDE = "SOCIAL & SPORT PROGRAM";
    private static String TAG_PROACOMP = "COMPANION PROGRAM";
    private static String TAG_INSCHOSP = "REGISTRATION & ACCOMMODATION";
    private static String TAG_SCANNERR = "SCANNER";
    private static String TAG_BTNMAPAS = "MAPS";
    private static String TAG_BTNFOTOS = "PHOTOS";
    private static String TAG_ENCUESTA = "SURVEYS";
    private static String TAG_DESCARGA = "DOWNLOADS";
    private static String TAG_DIARY = "DIARY";
    private static String TAG_MYEXHIBITORS = "MY EXHIBITORS";
    private static String TAG_TRANSPORTATION = "TRANSPORTATION";
    private static String TAG_PLACESOFINTEREST = "PLACE OF INTEREST";





    private static final String tabla_PROGRAMA = "program";
    private static final String tabla_PATROCIN = "patrocinador";
    private static final String tabla_EXPOSITO = "expositor";
    private static final String tabla_PROSOYDE = "deportivo";
    private static final String tabla_PROACOMP = "acompanante";
    private static final String tabla_INSCHOSP = "inscripciones";
    private static final String tabla_SCANNERR = "SCANNER";
    private static final String tabla_BTNMAPAS = "MAPAS";
    private static final String tabla_BTNFOTOS = "img";
    private static final String tabla_ENCUESTA = "encuesta";
    private static final String tabla_DESCARGA = "descargas";

    private static String LENGUAJE = "";
    String ps;

    private static int AnchoImagen;
    private static int LargoImagen;
    CargaBanner cb= new CargaBanner();
    ImageButton imgbbanner;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cmp);

        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

         sp = getSharedPreferences("prefe", CMP.MODE_PRIVATE);
        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(CMP.this));


        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

       /* File myDir =  new File(MEDIA_PATH_REGISTRO);
        Intent i = getIntent();
            ps = i.getStringExtra("paso");
        Log.d("hola","hola"+ ps);
        if(ps == null) {
            if (!myDir.exists()) {

                Intent intent = new Intent(getApplicationContext(), Registro.class);
                startActivity(intent);
            }
        }*/

        LENGUAJE = Locale.getDefault().getDisplayLanguage();
        Log.d("Lenguaje:", LENGUAJE);

        ImageButton BTNIMG1 = (ImageButton) findViewById(R.id.imgBtn1);
        ImageButton BTNIMG2 = (ImageButton) findViewById(R.id.imgBtn2);
        ImageButton BTNIMG3 = (ImageButton) findViewById(R.id.imgBtn3);
        ImageButton BTNIMG4 = (ImageButton) findViewById(R.id.imgBtn4);
        ImageButton BTNIMG5 = (ImageButton) findViewById(R.id.imgBtn5);
        ImageButton BTNIMG6 = (ImageButton) findViewById(R.id.imgBtn6);
        ImageButton BTNIMG7 = (ImageButton) findViewById(R.id.imgBtn7);
        ImageButton BTNIMG8 = (ImageButton) findViewById(R.id.imgBtn8);
        ImageButton BTNIMG9 = (ImageButton) findViewById(R.id.imgBtn9);
        ImageButton BTNIMG10 = (ImageButton) findViewById(R.id.imgBtn10);
        ImageButton BTNIMG11 = (ImageButton) findViewById(R.id.imgBtn11);
        ImageButton BTNIMG12 = (ImageButton) findViewById(R.id.imgBtn12);
        ImageButton BTNIMG13 = (ImageButton) findViewById(R.id.imgBtn13);
        ImageButton BTNIMG14 = (ImageButton) findViewById(R.id.imgBtn14);
        ImageButton BTNIMG15 = (ImageButton) findViewById(R.id.imgBtn15);

        if (ps!= null){
            if(ps.equals("2")){

            }else{

                BTNIMG1.setImageResource(R.drawable.menu_conferences_enable);
                BTNIMG1.setEnabled(false);
                BTNIMG3.setImageResource(R.drawable.menu_exposicionindustrial_enable);
                BTNIMG3.setEnabled(false);
                BTNIMG6.setImageResource(R.drawable.menu_programasocialydeportivo_enable);
                BTNIMG6.setEnabled(false);
                BTNIMG4.setImageResource(R.drawable.menu_agenda_enable);
                BTNIMG4.setEnabled(false);
                BTNIMG5.setImageResource(R.drawable.menu_misexpositores_enable);
                BTNIMG5.setEnabled(false);
                BTNIMG7.setImageResource(R.drawable.menu_acompanantes_enable);
                BTNIMG7.setEnabled(false);
                BTNIMG13.setImageResource(R.drawable.menu_encuestas_enable);
                BTNIMG13.setEnabled(false);
            }

        }
        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
            TAG_PROGRAMA = "CONFERENCIAS";
            TAG_PATROCIN = "PATROCINADORES";
            TAG_EXPOSITO = "EXPOSICÓN INDUSTRIAL";
            TAG_PROSOYDE = "PROGRAMA SOCIAL & DEPORTIVO";
            TAG_PROACOMP = "PROGRAMA ACOMPAÑANTES";
            TAG_INSCHOSP = "INSCRIPCION & HOSPEDAJE";
            TAG_SCANNERR = "SCANNER";
            TAG_BTNMAPAS = "MAPAS";
            TAG_BTNFOTOS = "FOTOS";
            TAG_ENCUESTA = "ENCUESTAS";
            TAG_DESCARGA = "DESCARGAS";
            TAG_DIARY = "AGENDA";
            TAG_MYEXHIBITORS = "MIS EXPOSITORES";
            TAG_TRANSPORTATION = "TRANSPORTACION";
            TAG_PLACESOFINTEREST = "SITIOS DE INTERES";
        } else {
            idioma="Ingles";
            BTNIMG1.setImageResource(R.drawable.menu_conferences);
            BTNIMG2.setImageResource(R.drawable.menu_sponsors);
            BTNIMG3.setImageResource(R.drawable.menu_exposition_industrial);
            BTNIMG4.setImageResource(R.drawable.menu_diary);
            BTNIMG5.setImageResource(R.drawable.menu_myexhibitors);
            BTNIMG6.setImageResource(R.drawable.menu_socialprogram);
            BTNIMG7.setImageResource(R.drawable.menu_companions);
            BTNIMG8.setImageResource(R.drawable.menu_inscriptionandlodging);
            BTNIMG9.setImageResource(R.drawable.menu_map);
            BTNIMG10.setImageResource(R.drawable.menu_photos);
            BTNIMG11.setImageResource(R.drawable.menu_placesofinterest);
            BTNIMG12.setImageResource(R.drawable.menu_transportation);
            BTNIMG13.setImageResource(R.drawable.menu_surveys);
            BTNIMG14.setImageResource(R.drawable.menu_downloads);
            BTNIMG15.setImageResource(R.drawable.menu_scanner);
        }

        cd = new ConnectionDetector(getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(CMP.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
       imgbbanner = (ImageButton)findViewById(R.id.Banner);
        conta();
        // Dentro de 0 milisegundos avísame cada 1000 milisegundos







        findViewById(R.id.imgBtn1).setOnClickListener(new ActionButton1());//Conferencias
        findViewById(R.id.imgBtn2).setOnClickListener(new ActionButton2());//Patrocinadores
        findViewById(R.id.imgBtn3).setOnClickListener(new ActionButton3());//Exposiciones industriales
        findViewById(R.id.imgBtn4).setOnClickListener(new ActionButton13());//Agenda
        findViewById(R.id.imgBtn5).setOnClickListener(new ActionButton12());//Mis expositores
        findViewById(R.id.imgBtn6).setOnClickListener(new ActionButton4());//Programa Social y Deportivo
        findViewById(R.id.imgBtn7).setOnClickListener(new ActionButton5());//Acompañante
        findViewById(R.id.imgBtn8).setOnClickListener(new ActionButton6());//Inscripciones y hospedajes
        findViewById(R.id.imgBtn9).setOnClickListener(new ActionButton8());//Mapa
        findViewById(R.id.imgBtn10).setOnClickListener(new ActionButton9());//foto
        findViewById(R.id.imgBtn11).setOnClickListener(new ActionButton14());//Sitios de interes
        findViewById(R.id.imgBtn12).setOnClickListener(new ActionButton15());//Transportacion
        findViewById(R.id.imgBtn13).setOnClickListener(new ActionButton10());//Encuestas
        findViewById(R.id.imgBtn14).setOnClickListener(new ActionButton11());//Descargas
        findViewById(R.id.imgBtn15).setOnClickListener(new ActionButton7());//Scanner

        // Obtain a reference to the mobile client. It is created in the Application class,
        // but in case a custom Application class is not used, we initialize it here if necessary.
        AWSMobileClient.initializeMobileClientIfNecessary(this);

        // Obtain a reference to the mobile client. It is created in the Application class.
        final AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        // Obtain a reference to the identity manager.
        identityManager = awsMobileClient.getIdentityManager();

    }

    @Override
    protected void onResume() {
        super.onResume();


        final AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        // register notification receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(notificationReceiver,
                new IntentFilter(PushListenerService.ACTION_SNS_NOTIFICATION));
    }

    private final BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(LOG_TAG, "Received notification from local broadcast. Display it in a dialog.");

            Bundle data = intent.getBundleExtra(PushListenerService.INTENT_SNS_NOTIFICATION_DATA);
            String message = PushListenerService.getMessage(data);

            new AlertDialog.Builder(CMP.this)
                    .setTitle(getString(R.string.push_title))
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        }
    };

    @Override
    protected void onPause() {
        super.onPause();

        // unregister notification receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationReceiver);
    }

public void conta(){
    Timer timer = new Timer();
    timer.scheduleAtFixedRate(timerTask, 0, 3000);
}
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(CMP.this));
                }
            });

        }
    };

    @Override
    public void onWindowFocusChanged(boolean hasFocus){

        image = (ImageView) findViewById(R.id.Banner);
        AnchoImagen = image.getWidth();
        LargoImagen = image.getHeight();

        Log.d("ancho banner", String.valueOf(AnchoImagen));
        Log.d("largo banner", String.valueOf(LargoImagen));

    }

    @Override
    public void onDestroy()
    {
        System.exit(0);
        super.onDestroy();
        this.finish();
    }



    class ActionButton1 implements OnClickListener {//conferencias
        public void onClick(View v) {


            String myIntValue = sp.getString("Conferencia","no");
            if(myIntValue.equals("si")){
                Intent intent = new Intent(getApplicationContext(), FiltroProgramEncuesta.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                intent.putExtra("tabla", tabla_PROGRAMA);
                intent.putExtra("idio",idioma);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                intent.putExtra("tabla", tabla_PROGRAMA);
                intent.putExtra("idio",idioma);
                startActivity(intent);
            }




        }
    }

    class ActionButton2 implements OnClickListener {//Patrocinadores
        public void onClick(View v) {
            //if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
            String myIntValue = sp.getString("Patrocinadores","no");
            if(myIntValue.equals("si")){

                Intent intent = new Intent(getApplicationContext(), Patrocinadores.class);
                intent.putExtra("TITULO", TAG_PATROCIN);
                intent.putExtra("tabla", tabla_PATROCIN);
                intent.putExtra("idio",idioma);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_PATROCIN);
                intent.putExtra("tabla", tabla_PATROCIN);
                intent.putExtra("idio",idioma);
                startActivity(intent);
            }

        }
    }

    class ActionButton3 implements OnClickListener {//exposiciones industriales
        public void onClick(View v) {
            String myIntValue = sp.getString("ExpoIndu","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
                intent.putExtra("TITULO", TAG_EXPOSITO);
                intent.putExtra("tabla", tabla_EXPOSITO);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_EXPOSITO);
                intent.putExtra("tabla", tabla_EXPOSITO);
                startActivity(intent);
            }

        }
    }


    class ActionButton4 implements OnClickListener { //Programa Social y Deportivo
        public void onClick(View v) {
            String myIntValue = sp.getString("SociDepor","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), ProgramSociaDepor.class);
                intent.putExtra("TITULO", TAG_PROSOYDE);
                intent.putExtra("tabla", tabla_PROSOYDE);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_PROSOYDE);
                intent.putExtra("tabla", tabla_PROSOYDE);
                startActivity(intent);
            }

        }
    }

    class ActionButton5 implements OnClickListener {//Acompañante
        public void onClick(View v) {
            String myIntValue = sp.getString("Acomp","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), ProgramSociaDepor.class);
                intent.putExtra("TITULO", TAG_PROACOMP);
                intent.putExtra("tabla", tabla_PROACOMP);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_PROACOMP);
                intent.putExtra("tabla", tabla_PROACOMP);
                startActivity(intent);
            }

        }
    }

    class ActionButton6 implements OnClickListener { //Inscripciones y hospedajes
        public void onClick(View v) {
            String myIntValue = sp.getString("InsHosp","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), HospeInsc.class);
                intent.putExtra("TITULO", TAG_INSCHOSP);
                intent.putExtra("tabla", tabla_INSCHOSP);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_INSCHOSP);
                intent.putExtra("tabla", tabla_INSCHOSP);
                startActivity(intent);
            }

        }
    }

    class ActionButton7 implements OnClickListener {//Scanner
        public void onClick(View v) {
            String myIntValue = sp.getString("Scanner","no");
            if(myIntValue.equals("si")) {
            Intent intent = new Intent(getApplicationContext(), SimpleScannerActivity.class);
            startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);

                intent.putExtra("tabla", tabla_SCANNERR);
                startActivity(intent);
            }
        }
    }

    class ActionButton8 implements OnClickListener {//Mapa
        public void onClick(View v) {
            String myIntValue = sp.getString("Mapa","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), FiltroMapa.class);
                intent.putExtra("TITULO", TAG_BTNMAPAS);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_BTNMAPAS);
                intent.putExtra("tabla", tabla_BTNMAPAS);
                startActivity(intent);
            }

        }
    }

    class ActionButton9 implements OnClickListener {//foto
        public void onClick(View v) {
            String myIntValue = sp.getString("Foto","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), MenuPhoto.class);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("tabla", "Foto");
                startActivity(intent);


            }
        }
    }

    class ActionButton10 implements OnClickListener {// encuesta
        public void onClick(View v) {
            String myIntValue = sp.getString("Encu","no");
            if(myIntValue.equals("si")) {
                if ("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
                    Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
                    intent.putExtra("TITULO", TAG_ENCUESTA);
                    intent.putExtra("tabla", tabla_ENCUESTA);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
                    intent.putExtra("TITULO", TAG_ENCUESTA);
                    intent.putExtra("tabla", tabla_ENCUESTA);
                    startActivity(intent);
                }
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_ENCUESTA);
                intent.putExtra("tabla", tabla_ENCUESTA);
                startActivity(intent);
            }
        }
    }

    class ActionButton11 implements OnClickListener { //Descarga
        public void onClick(View v) {
            String myIntValue = sp.getString("Desc","no");
            if(myIntValue.equals("si")) {

                    Intent intent = new Intent(getApplicationContext(), CursosPreCongre.class);

                    startActivity(intent);

            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", TAG_DESCARGA);
                intent.putExtra("tabla", tabla_DESCARGA);
                startActivity(intent);
            }
        }
    }
    class ActionButton12 implements OnClickListener {//Mis Expositores
        public void onClick(View v) {
            String myIntValue = sp.getString("MisExp","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), Buscar.class);
                LENGUAJE = Locale.getDefault().getDisplayLanguage();
                if ("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
                    intent.putExtra("TITULO", "MIS EXPOSITORES");
                } else {
                    intent.putExtra("TITULO", "MY EXHIBITORS");
                }
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", "MIS EXPOSITORES");
                intent.putExtra("tabla", "MisExp");
                startActivity(intent);
            }

        }
    }
    class ActionButton13 implements OnClickListener {//Agenda
        public void onClick(View v) {
            String myIntValue = sp.getString("Agenda","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), Agenda.class);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", "AGENDA");
                intent.putExtra("tabla", "Agenda");
                startActivity(intent);
            }
        }
    }
//placeholder for screen 4 mgarza delete after implementing (SITIOS DE INTERES)//
    class ActionButton14 implements OnClickListener {
        public void onClick(View v) {
            String myIntValue = sp.getString("SitInt","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), Sitios_Instrst.class);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", "Sitios de interes");
                intent.putExtra("tabla", "SitInt");
                startActivity(intent);
            }
        }
    }
    class ActionButton15 implements OnClickListener {//Transportacion
        public void onClick(View v) {
            String myIntValue = sp.getString("Trans","no");
            if(myIntValue.equals("si")) {
                Intent intent = new Intent(getApplicationContext(), Transportacion.class);
                startActivity(intent);
           }else{
                Intent intent = new Intent(getApplicationContext(), Tutorial.class);
                intent.putExtra("TITULO", "Transportacion");
                intent.putExtra("tabla", "Trans");
                startActivity(intent);
            }
        }
    }

}
