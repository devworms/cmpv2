package com.jonathan.cmp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salva on 10/05/2016.
 */
public class MapaExpo extends Activity{
    CargaBanner cb= new CargaBanner();
    WebView wb;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_expo);
        ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
        conta();
        wb= (WebView)findViewById(R.id.webMapa);
        WebSettings wbs=wb.getSettings();
        wbs.setBuiltInZoomControls(true);
        wbs.setJavaScriptEnabled(true);
        new LoadSingleTrack().execute();


    }
    class LoadSingleTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        /**
         * getting song json and parsing
         * */
        protected String doInBackground(String... args) {
            // Building Parameters

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting song information

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {



                    wb.setWebViewClient(new WebViewClient() {
                        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                        }

                        @Override
                        public void onPageStarted(WebView view, String url, Bitmap favicon)
                        {

                        }


                        @Override
                        public void onPageFinished(WebView view, String url) {
                            String webUrl = wb.getUrl();

                        }

                    });
                    Intent i = getIntent();
                    String url= i.getStringExtra("TITULO");
                    Log.d("url", url);
                    TextView txtTitu= (TextView)findViewById(R.id.txtTitulo);

                    if(url.equals("Expo")) {

                        txtTitu.setText("MAPA DE EXPOSITOR");
                        wb.loadUrl("http://docs.google.com/gview?embedded=true&url="+"http://app-ecodsa.com.mx/APP/plano1.pdf");

                    }else{
                        txtTitu.setText("MAPA DEL RECINTO");
                        wb.loadUrl("http://docs.google.com/gview?embedded=true&url="+"http://app-ecodsa.com.mx/APP/MapaDeRecinto.pdf");
                    }

                }
            });

        }

    }




    public void openPdf()

    {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        String path =  Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/";

        File file = new File(path, "pdf1.pdf");

        intent.setDataAndType( Uri.fromFile( file ), "application/pdf" );

        startActivity(intent);

    }


    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
}
