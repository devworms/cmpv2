package com.jonathan.cmp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by salvador on 25/03/2016.
 */
public class AdminSQLiteAgenda extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "agenda.db";
    public static final String TABLA_PERSONAS = "calendario";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NOMBRE = "titulo";
    public static final String COLUMN_CONFERENCISTA = "conferencista";
    public static final String COLUMN_DESCRIP = "descripcion";
    public static final String COLUMN_LUGAR = "lugar";
    public static final String COLUMN_FECHA = "fecha";
    public static final String COLUMN_HORA = "hora";
    public static final String COLUMN_H = "hora";
    public static final String COLUMN_DESCRIPEVENT = "descEvent";



    public AdminSQLiteAgenda(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLA_PERSONAS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NOMBRE + " TEXT, " +
                COLUMN_CONFERENCISTA + " TEXT, " +
                COLUMN_DESCRIP + " TEXT, " +
                COLUMN_LUGAR + " TEXT, " +
                COLUMN_FECHA + " TEXT, " +
                COLUMN_HORA + " TEXT, " +
                COLUMN_DESCRIPEVENT + " TEXT " +

                ");";

        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PERSONAS);
        onCreate(db);
    }

    //Añade un nuevo Row a la Base de Datos

    public void addEvento(String titulo, String conferencista, String descri, String lugar, String fecha, String hr,String event) {
        if (hr.length()<=4){
            hr="0"+hr;
        }
        ContentValues values = new ContentValues();
        values.put(COLUMN_NOMBRE, titulo);
        values.put(COLUMN_CONFERENCISTA, conferencista);
        values.put(COLUMN_DESCRIP, descri);
        values.put(COLUMN_LUGAR, lugar);
        values.put(COLUMN_FECHA, fecha);
        values.put(COLUMN_HORA, hr);
        values.put(COLUMN_DESCRIPEVENT,event);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLA_PERSONAS, null, values);
        db.close();

    }



    // Borrar una persona de la Base de Datos

    public void borrarEvent(String persona_id){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLA_PERSONAS + " WHERE " + COLUMN_ID + " = " + persona_id + ";");
        db.close();
    }





    //listar a todas las personas
    public Cursor listarpersonas(){

        SQLiteDatabase db = getReadableDatabase();
        String query = ("SELECT * FROM " + TABLA_PERSONAS + " ORDER BY "+COLUMN_DESCRIP+" ASC,"+ COLUMN_HORA +" ASC;");// WHERE "+COLUMN_FECHA+" LIKE '%"+dia+" "+mesNom+"%' ORDER BY "+ COLUMN_HORA +" ASC;");
        Cursor c = db.rawQuery(query, null);

        if (c != null) {
            c.moveToFirst();
        }

        return c;
    }
    public Cursor listarpersonasid(Integer id){
        SQLiteDatabase db = getReadableDatabase();
        String query = ("SELECT * FROM " + TABLA_PERSONAS + " where _id =  '"+ id +"';");
        Cursor c = db.rawQuery(query, null);

        if (c != null) {
            c.moveToFirst();
        }

        return c;
    }
    public Cursor agendaPush(){
        SQLiteDatabase db = getReadableDatabase();
        String query = ("SELECT "+COLUMN_FECHA+", "+ COLUMN_HORA+","+COLUMN_ID+" FROM " + TABLA_PERSONAS +";");
        Cursor c = db.rawQuery(query, null);

        if (c != null) {
            c.moveToFirst();
        }

        return c;
    }
}