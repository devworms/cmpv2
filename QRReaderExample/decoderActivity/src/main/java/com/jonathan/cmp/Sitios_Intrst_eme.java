package com.jonathan.cmp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by mgarza on 03/05/2016.
 */
public class Sitios_Intrst_eme extends Activity {
    CargaBanner cb = new CargaBanner();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sitios_intrst_eme);
        conta();
        ImageButton imgbbanner = (ImageButton) findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(Sitios_Intrst_eme.this));
        findViewById(R.id.btnPolicia).setOnClickListener(new Policia());
        findViewById(R.id.btnAmb).setOnClickListener(new Ambulancia());
        findViewById(R.id.btnBom).setOnClickListener(new Bomberos());
        findViewById(R.id.btnConsulExpo).setOnClickListener(new ConsulExpo());


    }

    class Policia implements View.OnClickListener {
        public void onClick(View v) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:81259400"));

            startActivity(callIntent);

        }
    }
    class Ambulancia implements View.OnClickListener {
        public void onClick(View v) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:14771477"));

            startActivity(callIntent);

        }
    }
    class Bomberos implements View.OnClickListener {
        public void onClick(View v) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:8342-0053"));

            startActivity(callIntent);

        }
    }
    class ConsulExpo implements View.OnClickListener {
        public void onClick(View v) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:8183696900"));

            startActivity(callIntent);

        }
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };

}
