package com.jonathan.cmp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salva on 23/05/2016.
 */
public class TransporVuelos extends Activity {
    CargaBanner cb= new CargaBanner();
    String tipo;
    WebView wb,wb2;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transvuelo);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(TransporVuelos.this));
        wb=(WebView) findViewById(R.id.wbvTrans);
        WebSettings wbs=wb.getSettings();
        wbs.setBuiltInZoomControls(true);
        wbs.setJavaScriptEnabled(true);

        wb2=(WebView) findViewById(R.id.wbvTrans2);

        WebSettings wbs2=wb2.getSettings();
        wbs2.setBuiltInZoomControls(true);
        wbs2.setJavaScriptEnabled(true);
        conta();

        new CargaPdf().execute();

    }
    class CargaPdf extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        /**
         * getting song json and parsing
         * */
        protected String doInBackground(String... args) {
            // Building Parameters

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting song information

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {



                    wb.setWebViewClient(new WebViewClient() {
                        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                        }

                        @Override
                        public void onPageStarted(WebView view, String url, Bitmap favicon)
                        {

                        }


                        @Override
                        public void onPageFinished(WebView view, String url) {
                            String webUrl = wb.getUrl();


                        }

                    });
                    wb2.setWebViewClient(new WebViewClient() {
                        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                        }

                        @Override
                        public void onPageStarted(WebView view, String url, Bitmap favicon)
                        {

                        }


                        @Override
                        public void onPageFinished(WebView view, String url) {
                            String webUrl = wb2.getUrl();


                        }

                    });


                        wb.loadUrl("http://docs.google.com/gview?embedded=true&url=" + "http://app-ecodsa.com.mx/APP/7_8.pdf");

                        wb2.loadUrl("http://docs.google.com/gview?embedded=true&url=" + "http://app-ecodsa.com.mx/APP/11_12.pdf");


                }
            });

        }

    }

    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
}
