package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by salvador on 05/04/2016.
 */

public class HospeInsc extends Activity {
    String TAG_INSCHOSP = "INSCRIPCION & HOSPEDAJE";
    private static final String tabla_INSCHOSP = "inscripciones";
    private static String LENGUAJE = "";

    CargaBanner cb= new CargaBanner();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inshos);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(HospeInsc.this));
        conta();
        findViewById(R.id.btnCollh).setOnClickListener(new ActionButton1());//Conferencias
        findViewById(R.id.btnInscrip).setOnClickListener(new WebviewactivityInscripcion());
        findViewById(R.id.btnHospe).setOnClickListener(new WebviewactivityHospe());

        LENGUAJE = Locale.getDefault().getDisplayLanguage();
        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {}
        else{
            TAG_INSCHOSP="INSCRIPTION AND LODGING";
        }


    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    class ActionButton1 implements View.OnClickListener {
        public void onClick(View v) {
           // if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
                Intent intent = new Intent(getApplicationContext(), ComoLlegarHotel.class);

                startActivity(intent);
            //}
        }
    }
    class WebviewactivityInscripcion implements View.OnClickListener {
        public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), WebviewInsHosp.class);
                intent.putExtra("Tipo", "http://www.congresomexicanodelpetroleo.com.mx/inscripcion.html");
                intent.putExtra("TITULO", TAG_INSCHOSP);
                startActivity(intent);

        }
    }
    class WebviewactivityHospe implements View.OnClickListener {
        public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), WebviewInsHosp.class);
                intent.putExtra("Tipo", "http://www.congresomexicanodelpetroleo.com.mx/hospedaje.html");
                intent.putExtra("TITULO", TAG_INSCHOSP);
                startActivity(intent);

        }
    }


}
