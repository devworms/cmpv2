package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 29/01/15.
 */

public class FiltroProgramEncuesta extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    //

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    private Spinner spinner1, spinner2,spSalas;
    private Button btnSubmit;

    String TITULO;
    String tabla;
    String idioma;
    Drawable dra;
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_program_encuesta);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(FiltroProgramEncuesta.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
          //  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

       // new LoadBanners().execute();
        addItemsOnSpinner1();
        addItemsOnSpinner2();
        addItemsOnSpinner3();
        addListenerOnButton();


        Intent i = getIntent();
        TITULO = i.getStringExtra("TITULO");
        tabla = i.getStringExtra("tabla");
        idioma= i.getStringExtra("idio");
        if("program".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_conferencias);

        }else if("encuesta".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_encuestas);

        }

        ImageView ImageTitu = (ImageView) findViewById(R.id.imvTitu);
        ImageTitu.setImageDrawable(dra);
        TextView Titulo = (TextView) findViewById(R.id.txtTitulo);

        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        Titulo.setTypeface(font);
        TextView Descripcion = (TextView) findViewById(R.id.txtDescrip);

        Titulo.setText(TITULO);
        Descripcion.setText("Explora por día o categoría todos los eventos del congreso.");
        if("Ingles".equals(idioma)) {
            Descripcion.setText("Explore per day or category all the congress events.");
            btnSubmit = (Button) findViewById(R.id.btnSubmit);
            btnSubmit.setText("SEARCH");
        }else{

            Descripcion.setText("Explora por día o categoría todos los eventos del congreso.");
        }
    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    // add items into spinner dynamically
    public void addItemsOnSpinner2() {

        spinner2 = (Spinner) findViewById(R.id.spinner2);
        List<String> list = new ArrayList<String>();
        list.add("Todos");
        list.add("Comidas Conferencias");
        list.add("Plenarias");
        list.add("Sesiones Técnicas");
        list.add("E-Póster");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter);
        spinner2.setPrompt("tipo de evento");
    }

    public void addItemsOnSpinner1() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        List<String> list = new ArrayList<String>();
        list.add("Todos");
        list.add("Miercoles 8");
        list.add("Jueves 9");
        list.add("Viernes 10");
        list.add("Sabado 11");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
        spinner1.setPrompt("fecha");

    }
    public void addItemsOnSpinner3() {

        spSalas = (Spinner) findViewById(R.id.spSalas);
        List<String> list = new ArrayList<String>();
        list.add("Todas");
        list.add("Monterrey");
        list.add("Mexico");
        list.add("Cd Carmen");
        list.add("Reynosa");
        list.add("Villahermosa");
        list.add("Tampico");
        list.add("Poza Rica");
        list.add("Comalcalco");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalas.setAdapter(dataAdapter);
        spSalas.setPrompt("Salas");
    }

    // get the selected dropdown list value
    public void addListenerOnButton() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        spSalas = (Spinner) findViewById(R.id.spSalas);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
                intent.putExtra("TITULO", TITULO);
                intent.putExtra("tabla", tabla);
                intent.putExtra("fecha", String.valueOf(spinner1.getSelectedItem()));
                intent.putExtra("tipo", String.valueOf(spinner2.getSelectedItem()));
                intent.putExtra("sala", String.valueOf(spSalas.getSelectedItem()));
                intent.putExtra("idio",idioma);
                startActivity(intent);

            }

        });
    }



}