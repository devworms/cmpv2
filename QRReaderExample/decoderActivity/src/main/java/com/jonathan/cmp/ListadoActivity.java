package com.jonathan.cmp;

/**
 * Created by lhuu on 27/01/15.
 */


import android.app.ListActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ListadoActivity extends ListActivity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();



    ArrayList<String> NotificationList = new ArrayList<String>();
    ArrayList<Integer> NotificationIDList = new ArrayList<Integer>();
    private static final String MEDIA_PATH_NOTIFI = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/Notificaciones/";
    private static final String URL_NOTIFI = "http://app-ecodsa.com.mx/APP/alerta.php";
    private static final String TAG_ID_NOTIFI = "id";
    private static final String TAG_NAME_NOTIFI = "alerta";


    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    String TITULO;
    String tabla;
    String FECHA;
    String TIPO;
    String idioma;
    String Sala;
    Drawable dra;
    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    ArrayList<HashMap<String, String>> albumsList;

    // albums JSONArray
    JSONArray albums = null;
    private static final int NOTIFICATION_ID=1;


    // albums JSON url
    private static final String URL_ALBUMS = "http://app-ecodsa.com.mx/APP/Listado.php";
    private static final String URL_ALBUMS_TWO = "http://app-ecodsa.com.mx/APP/Listado2.php";

    // ALL JSON node names
    String TAG_ID = "id";
    String TAG_NAME = "ne";
    String TAG_FECHA = "fecha";
    String TAG_NOMBRE = "nombre";
    String TAG_NIVEL = "nivel";
    String TAG_EVENTO = "evento";
    String TAG_TIPO = "tipo";
    String TAG_URL = "le";
    String TAG_ICONO = "icono";
    String TAG_IMAGE = "img";
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);

        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);


        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(ListadoActivity.this));
        conta();

        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        cd = new ConnectionDetector(getApplicationContext());

        // Check for internet connection
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(ListadoActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }



       // new LoadBanners().execute();

        // Get album id, song id
        Intent i = getIntent();
        TITULO = i.getStringExtra("TITULO");
        tabla = i.getStringExtra("tabla");
        FECHA = i.getStringExtra("fecha");
        TIPO = i.getStringExtra("tipo");
        idioma= i.getStringExtra("idio");
        Sala =i.getStringExtra("sala");
        if("Ingles".equals(idioma)){
            TAG_ID = "id";
            TAG_NAME = "ni";
            TAG_FECHA = "fecha";
            TAG_NOMBRE = "nombre";
            TAG_NIVEL = "nivel";
            TAG_EVENTO = "eventoi";
            TAG_TIPO = "tipo";
            TAG_URL = "li";
        }
        Log.d("TITULO:", TITULO);
        Log.d("tabla:", tabla);
        if("program".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_conferencias);

        }else if("patrocinador".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_patrocinadores);

        }else if("expositor".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_expositores);

        }else if("deportivo".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_programasocial);

        }else if("acompanante".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_acompanantes);

        }else if("inscripciones".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_inscripcionyhospedaje);

        }else if("MAPAS".equals(TITULO)){
            dra=this.getResources().getDrawable(R.drawable.seccion_mapa);

        }else if("encuesta".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_encuestas);

        }else if("descargas".equals(tabla)){
            dra=this.getResources().getDrawable(R.drawable.seccion_descargas);

        }


        ImageView ImageTitu = (ImageView) findViewById(R.id.imvTitu);
        ImageTitu.setImageDrawable(dra);



        AutoCompleteTextView acTextView = (AutoCompleteTextView) findViewById(R.id.autoComplete);
        //acTextView.setAdapter(new SuggestionAdapter(this,acTextView.getText().toString(),tabla ));
        //acTextView.setOnItemClickListener(ListadoActivity.this);
        if("deportivo".equals(tabla) || "acompanante".equals(tabla)|| "patrocinador".equals(tabla)){
            acTextView.setVisibility(View.INVISIBLE);
        }
        acTextView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                albums = null;
                albumsList.clear();
                new LoadAlbums2().execute();
            }
        });


        TextView Titulo = (TextView) findViewById(R.id.txtTitulo);

        Typeface font= Typeface.createFromAsset(getAssets(), "segoewp.ttf");
        Titulo.setTypeface(font);
        Titulo.setText(TITULO);

        // Hashmap for ListView
        albumsList = new ArrayList<HashMap<String, String>>();

        // Loading Albums JSON in Background Thread
        new LoadAlbums().execute();

        // get listview
        ListView lv = getListView();

        /**
         * Listview item click listener
         * TrackListActivity will be lauched by passing album id
         * */
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int arg2,
                                    long arg3) {
                // on selecting a single album
                // TrackListActivity will be launched to show tracks inside the album
                if("patrocinador".equals(tabla) || "expositor".equals(tabla)) {
                    Intent i = new Intent(getApplicationContext(), PatrocinExposiActivity.class);
                    // send album id to tracklist activity to get list of songs under that album
                    String album_id = ((TextView) view.findViewById(R.id.album_id)).getText().toString();
                    i.putExtra("ID", album_id);
                    i.putExtra("tabla", tabla);
                    startActivity(i);
                } else if("deportivo".equals(tabla) || "acompanante".equals(tabla)) {
                    Intent i = new Intent(getApplicationContext(), DepoAcompActivity.class);
                    // send album id to tracklist activity to get list of songs under that album
                    String album_id = ((TextView) view.findViewById(R.id.album_id)).getText().toString();
                    i.putExtra("ID", album_id);
                    i.putExtra("tabla", tabla);
                    i.putExtra("titulo",TITULO);
                    startActivity(i);
                } else if("descargas".equals(tabla) || "inscripciones".equals(tabla)) {
                    String album_id = ((TextView) view.findViewById(R.id.album_id)).getText().toString();
                    Intent i = new Intent(Intent.ACTION_VIEW);

                    i.setData(Uri.parse(album_id));
                    startActivity(i);
                } else if("program_mapa".equals(tabla) || "expositor_mapa".equals(tabla) || "patrocinador_mapa".equals(tabla)) {
                    Intent i = new Intent(getApplicationContext(), MapaIDActivity.class);
                    // send album id to tracklist activity to get list of songs under that album
                    String album_id = ((TextView) view.findViewById(R.id.album_id)).getText().toString();
                    i.putExtra("ID", album_id);
                    i.putExtra("tabla", tabla);
                    i.putExtra("titulo",TITULO);
                    startActivity(i);
                } else if("encuesta".equals(tabla)){
                    Intent i = new Intent(getApplicationContext(), EncuestasDetail.class);
                    // send album id to tracklist activity to get list of songs under that album
                    String album_id = ((TextView) view.findViewById(R.id.album_id)).getText().toString();
                    i.putExtra("ID", album_id);
                    i.putExtra("tabla", tabla);
                    startActivity(i);
                } else {
                    Intent i = new Intent(getApplicationContext(), SingleTrackActivity.class);
                    // send album id to tracklist activity to get list of songs under that album
                    String album_id = ((TextView) view.findViewById(R.id.album_id)).getText().toString();
                    i.putExtra("ID", album_id);
                    i.putExtra("tabla", tabla);
                    startActivity(i);
                }
            }
        });
    }


    /**
     * Background Async Task to Load all Albums by making http request
     * */
    class LoadAlbums extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ListadoActivity.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("path", tabla));
            params.add(new BasicNameValuePair("fecha", FECHA));
            params.add(new BasicNameValuePair("tipo", TIPO));
            params.add(new BasicNameValuePair("sala", Sala));
            // getting JSON string from URL
            Log.d("Idioma: ", "> " + idioma);
            String lsUrl ="";

            if ("PLATINO".equals(TITULO)||"PLATINUM".equals(TITULO)) {
                lsUrl = "http://app-ecodsa.com.mx/APP/platino.php";
            } else if ("ORO".equals(TITULO)||"GOLD".equals(TITULO)) {
                lsUrl = "http://app-ecodsa.com.mx/APP/oro.php";
            } else if ("PLATA".equals(TITULO)||"SILVER".equals(TITULO)) {
                lsUrl = "http://app-ecodsa.com.mx/APP/plata.php";
            } else if ("BRONCE".equals(TITULO)||"BRONZE".equals(TITULO)) {
                lsUrl = "http://app-ecodsa.com.mx/APP/bronce.php";
            } else {
                lsUrl = "http://app-ecodsa.com.mx/APP/Listado.php";
            }

            String json = jsonParser.makeHttpRequest(lsUrl, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", "> " + json);

            try {
                albums = new JSONArray(json);

                if (albums != null) {
                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);


                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // Storing each json item values in variable
                        String id = c.getString(TAG_ID);
                        String name;
                        String fecha;
                        String icono = null;
                        if("patrocinador".equals(tabla) || "patrocinador_mapa".equals(tabla)) {
                            name = c.getString(TAG_NOMBRE);
                            fecha = c.getString(TAG_NIVEL);
                        } else if( "expositor".equals(tabla) || "expositor_mapa".equals(tabla) ) {
                            name = c.getString(TAG_NOMBRE);
                            fecha = "";
                        } else if("descargas".equals(tabla)) {
                            name = c.getString(TAG_NAME);
                            fecha = c.getString(TAG_URL);
                            icono = c.getString(TAG_ICONO);
                        } else if("inscripciones".equals(tabla) ) {
                            name = c.getString(TAG_NAME);
                            fecha = c.getString(TAG_URL);
                            icono = c.getString(TAG_IMAGE);
                        } else if( "encuesta".equals(tabla)) {
                            name = c.getString(TAG_EVENTO);
                            fecha = c.getString(TAG_TIPO);
                        } else if( "program_mapa".equals(tabla)) {
                            name = c.getString(TAG_NAME);
                            fecha = c.getString(TAG_FECHA);
                        } else {
                            name = c.getString(TAG_NAME);
                            fecha = c.getString(TAG_FECHA);
                        }
                        map.put(TAG_ID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_FECHA, fecha);
                        map.put(TAG_IMAGE, icono);
                        // adding HashList to ArrayList
                        albumsList.add(map);

                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    if("descargas".equals(tabla) || "inscripciones".equals(tabla) ) {
                        ListAdapter adapter = new SimpleAdapter(
                                ListadoActivity.this, albumsList,
                                R.layout.list_item_desc, new String[] { TAG_FECHA, TAG_FECHA, "track_no",
                                TAG_NAME, "drawable/"+TAG_IMAGE }, new int[] {
                                R.id.album_id, R.id.song_id, R.id.track_no, R.id.album_name, R.id.imageItem });
                        // updating listview
                        setListAdapter(adapter);
                    } else if("expositor".equals(tabla) || "expositor_mapa".equals(tabla) ){
                        ListAdapter adapter = new SimpleAdapter(
                                ListadoActivity.this, albumsList,
                                R.layout.list_item_listado, new String[] { TAG_ID,
                                TAG_NAME, TAG_FECHA }, new int[] {
                                R.id.album_id, R.id.second_name, R.id.songs_count });

                        // updating listview
                        setListAdapter(adapter);
                    } else {
                        ListAdapter adapter = new SimpleAdapter(
                                ListadoActivity.this, albumsList,
                                R.layout.list_item_listado, new String[] { TAG_ID,
                                TAG_NAME, TAG_FECHA }, new int[] {
                                R.id.album_id, R.id.album_name, R.id.songs_count });

                        // updating listview
                        setListAdapter(adapter);

                    }
                }
            });

        }

    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };

    /**
     * Background Async Task to Load all Albums by making http request
     * */
    class LoadAlbums2 extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            AutoCompleteTextView acTextView = (AutoCompleteTextView) findViewById(R.id.autoComplete);
            params.add(new BasicNameValuePair("text", acTextView.getText().toString()));
            params.add(new BasicNameValuePair("path", tabla));
            params.add(new BasicNameValuePair("fecha", FECHA));
            params.add(new BasicNameValuePair("tipo", TIPO));
            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_ALBUMS_TWO, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", "> " + json);

            try {
                albums = new JSONArray(json);

                if (albums != null) {
                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);


                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // Storing each json item values in variable
                        String id = c.getString(TAG_ID);
                        String name;
                        String fecha;
                        if("patrocinador".equals(tabla) || "patrocinador_mapa".equals(tabla)) {
                            name = c.getString(TAG_NOMBRE);
                            fecha = c.getString(TAG_NIVEL);
                        } else if( "expositor".equals(tabla) || "expositor_mapa".equals(tabla) ) {
                            name = c.getString(TAG_NOMBRE);
                            fecha = "";
                        } else if("descargas".equals(tabla) || "inscripciones".equals(tabla) ) {
                            name = c.getString(TAG_NAME);
                            fecha = c.getString(TAG_URL);
                        } else if( "encuesta".equals(tabla)) {
                            name = c.getString(TAG_EVENTO);
                            fecha = c.getString(TAG_TIPO);
                        } else if( "program_mapa".equals(tabla)) {
                            name = c.getString(TAG_NAME);
                            fecha = c.getString(TAG_FECHA);
                        } else {
                            name = c.getString(TAG_NAME);
                            fecha = c.getString(TAG_FECHA);
                        }
                        map.put(TAG_ID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_FECHA, fecha);
                        // adding HashList to ArrayList
                        albumsList.add(map);

                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    if("descargas".equals(tabla) || "inscripciones".equals(tabla) ) {
                        ListAdapter adapter = new SimpleAdapter(
                                ListadoActivity.this, albumsList,
                                R.layout.list_item_desc, new String[] { TAG_FECHA, TAG_FECHA, "track_no",
                                TAG_NAME }, new int[] {
                                R.id.album_id, R.id.song_id, R.id.track_no, R.id.album_name });
                        // updating listview
                        setListAdapter(adapter);
                    } else if("expositor".equals(tabla) || "expositor_mapa".equals(tabla) ){
                        ListAdapter adapter = new SimpleAdapter(
                                ListadoActivity.this, albumsList,
                                R.layout.list_item_listado, new String[] { TAG_ID,
                                TAG_NAME, TAG_FECHA }, new int[] {
                                R.id.album_id, R.id.second_name, R.id.songs_count });

                        // updating listview
                        setListAdapter(adapter);
                    } else {
                        ListAdapter adapter = new SimpleAdapter(
                                ListadoActivity.this, albumsList,
                                R.layout.list_item_listado, new String[] { TAG_ID,
                                TAG_NAME, TAG_FECHA }, new int[] {
                                R.id.album_id, R.id.album_name, R.id.songs_count });

                        // updating listview
                        setListAdapter(adapter);

                    }
                }
            });

        }

    }








}
