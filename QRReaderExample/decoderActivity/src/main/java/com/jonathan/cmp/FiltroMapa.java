package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lhuu on 30/01/15.
 */

public class FiltroMapa extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app.ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    //
    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    private Spinner spinner1, spinner2;
    private Button btnSubmit;

    String TITULO;
    String tabla;
    Drawable dra;
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_mapa);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(FiltroMapa.this));
        conta();
        Button btnMapaExpo=(Button)findViewById(R.id.btnMapExp);
        btnMapaExpo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {



                        /*Intent pantalla = new Intent(Intent.ACTION_VIEW,Uri.parse("http://app-ecodsa.com.mx/APP/plano1.pdf"));
                            startActivity(pantalla);*/


                Intent intent = new Intent(getApplicationContext(), MapaExpo.class);
                intent.putExtra("TITULO", "Expo");
                startActivity(intent);


            }

        });
        Button btnMapaRecin=(Button)findViewById(R.id.btnMapRec);
        btnMapaRecin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), MapaExpo.class);
                intent.putExtra("TITULO", "Reci");
                startActivity(intent);


            }

        });


    }
    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };
    public void addItemsOnSpinner1() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        List<String> list = new ArrayList<String>();
        list.add("Mapa Recinto");
        list.add("Mapa Expositores");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
        spinner1.setPrompt("tipo de mapas");
        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if("Mapa Recinto".equals(spinner1.getSelectedItem())){
                    addItemsOnSpinner3();
                } else {
                    addItemsOnSpinner2();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    // add items into spinner dynamically
    public void addItemsOnSpinner2() {

        spinner2 = (Spinner) findViewById(R.id.spinner2);
        List<String> list = new ArrayList<String>();
        list.add("Expositor");
        list.add("Patrocinador");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter);
        spinner2.setPrompt("contenido de mapas");

        addListenerOnButton();
    }


    // add items into spinner dynamically
    public void addItemsOnSpinner3() {

        spinner2 = (Spinner) findViewById(R.id.spinner2);
        List<String> list = new ArrayList<String>();
        list.add("Programa general");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter);
        spinner2.setPrompt("contenido de mapas");
        addListenerOnButton();
    }

    // get the selected dropdown list value
    public void addListenerOnButton() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if("Programa general".equals(spinner2.getSelectedItem())){
                    tabla = "program_mapa";
                } else if ("Expositor".equals(spinner2.getSelectedItem())){
                    tabla = "expositor_mapa";
                } else if ("Patrocinador".equals(spinner2.getSelectedItem())){
                    tabla = "patrocinador_mapa";
                }


                Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
                intent.putExtra("TITULO", TITULO);
                intent.putExtra("tabla", tabla);
                intent.putExtra("fecha", "");
                intent.putExtra("tipo", "");
                startActivity(intent);

            }

        });
    }


}