package com.jonathan.cmp;

/**
 * Created by lhuu on 27/01/15.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class PatrocinExposiActivity extends Activity {
    private static final String MEDIA_PATH_BANNER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/cel/";
    private static final String MEDIA_PATH_BANNER_TABLET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CMP/banners/tablet/";
    private File filePhoto = new File(MEDIA_PATH_BANNER);String nombre;
    private Timer timer;
    private static ImageView image;
    private static ImageButton imageButton;
    private static final String URL_BANNERS = "http://app-ecodsa.com.mx/APP/patrocinadores.php";
    private static final String TAG_ID_BANNER = "id";
    private static final String TAG_NAME_BANNER = "cel";
    private static final String TAG_NAME_TABLET = "tablet";
    private static final String TAG_BANNER_LINK = "nombre";

    ArrayList<String> BannerList = new ArrayList<String>();
    ArrayList<String> LinkBannerList = new ArrayList<String>();
    //
    // Connection detector
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogMnager alert = new AlertDialogMnager();

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // tracks JSONArray
    JSONArray albums = null;

    // Album id
    String ID;
    String tabla;
    ImageView iv;
    Bitmap imageBit ;

    String id, name, tel, email, de, img;
    String busca,nom;
    Integer pos;
    // single song JSON url
    // GET parameters album, song
    private static final String URL_SONG = "http://app-ecodsa.com.mx/APP/descripcion.php";

    // ALL JSON node names
     String TAG_ID = "id";
     String TAG_NAME = "nombre";
     String TAG_DESC = "de";
     String TAG_TEL = "telefono";
     String TAG_MAIL = "email";
     String TAG_IMAG = "img";
    CargaBanner cb= new CargaBanner();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patroexpos);


        String tabletSize = getResources().getString(R.string.screen_type);
        Log.d("pantalla", tabletSize);

        ImageButton imgbbanner= (ImageButton)findViewById(R.id.Banner);
        imgbbanner.setImageDrawable(cb.cargaimage(PatrocinExposiActivity.this));
        conta();
        if ("phone".equals(tabletSize)) {
            // do something
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // do something else
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(PatrocinExposiActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
        findViewById(R.id.btnAgreExpo).setOnClickListener(new ActionButton3());
        //new LoadBanners().execute();
        iv = (ImageView) findViewById(R.id.Logo);

        // Get album id, song id
        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        tabla = i.getStringExtra("tabla");
        busca=i.getStringExtra("busca");
        pos=i.getIntExtra("nom",0);
        String LENGUAJE = Locale.getDefault().getDisplayLanguage();
        if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {

        }else{
            TAG_ID = "id";
            TAG_NAME = "nombre";
            TAG_DESC = "di";
            TAG_TEL = "telefono";
            TAG_MAIL = "email";
            TAG_IMAG = "img";
        }


        if(busca!=null){
            TextView txt_name = (TextView) findViewById(R.id.name);
            TextView txt_tele = (TextView) findViewById(R.id.phone);
            TextView txt_mail = (TextView) findViewById(R.id.email);
            TextView txt_desc = (TextView) findViewById(R.id.description);
            Button btnagre=(Button)findViewById(R.id.btnAgreExpo);
            btnagre.setVisibility(View.INVISIBLE);
            AdminSQLiteOpenHelper dbHandler;
            dbHandler = new AdminSQLiteOpenHelper(this, null, null, 1);
            SQLiteDatabase db = dbHandler.getWritableDatabase();
            Cursor cursor = dbHandler.listarpersonasid(pos+1);
            txt_name.setText(cursor.getString(1));
            txt_tele.setText(cursor.getString(3));
            txt_mail.setText(cursor.getString(4));
            txt_desc.setText(cursor.getString(2));
            ID=cursor.getString(5);
            tabla="expositor";
            findViewById(R.id.btnLocalizar).setOnClickListener(new ActionButton2());
        }else{
        // calling background thread
        new LoadSingleTrack().execute();
        }
    }

    public void conta(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 3000);
    }
    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageButton  imgbbanner = (ImageButton)findViewById(R.id.Banner);
                    imgbbanner.setImageDrawable(cb.cargaimage(getBaseContext()));
                }
            });

        }
    };


    class ActionButton2 implements OnClickListener {
        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(), MapaIDActivity.class);
            // send album id to tracklist activity to get list of songs under that album
            i.putExtra("ID", ID);
            i.putExtra("tabla", tabla);
            startActivity(i);
        }
    }
    class ActionButton3 implements OnClickListener {
        public void onClick(View v) {
            AgregarExpo();
        }
    }
    /**
     * Background Async Task to get single song information
     * */
    public void AgregarExpo(){
        AdminSQLiteOpenHelper dbHandler;
        dbHandler = new AdminSQLiteOpenHelper(this, null, null, 1);
        SQLiteDatabase db = dbHandler.getWritableDatabase();
        TextView txt_name = (TextView) findViewById(R.id.name);
        TextView txt_tele = (TextView) findViewById(R.id.phone);
        TextView txt_mail = (TextView) findViewById(R.id.email);
        TextView txt_desc = (TextView) findViewById(R.id.description);
        dbHandler.addPersona( txt_name.getText().toString(),txt_desc.getText().toString(),txt_tele.getText().toString(),txt_mail.getText().toString(),ID);

        Toast.makeText(this, "Se guardo en Mis expositores",
                Toast.LENGTH_SHORT).show();
    }
    class LoadSingleTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PatrocinExposiActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting song json and parsing
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            // post album id, song id as GET parameters
            params.add(new BasicNameValuePair("id", ID));
            params.add(new BasicNameValuePair("tabla", tabla));

            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_SONG, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("Single Track JSON: ", json);

            try {
                JSONObject jObj = new JSONObject(json);
                if(jObj != null){
                    id = jObj.getString(TAG_ID);
                    name = jObj.getString(TAG_NAME);
                    tel = jObj.getString(TAG_TEL);
                    email = jObj.getString(TAG_MAIL);
                    de = jObj.getString(TAG_DESC);
                    img = jObj.getString(TAG_IMAG);
                }

                Log.d("img", img);
                imageBit = downloadBitmap("http://app-ecodsa.com.mx/patrocinadores/"+img);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting song information
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    String LENGUAJE = Locale.getDefault().getDisplayLanguage();
                    if("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
                        if(imageBit!=null)
                        {
                            iv.setImageBitmap(imageBit);
                        }

                        TextView txt_name = (TextView) findViewById(R.id.name);
                        TextView txt_tele = (TextView) findViewById(R.id.phone);
                        TextView txt_mail = (TextView) findViewById(R.id.email);
                        TextView txt_desc = (TextView) findViewById(R.id.description);

                        // displaying song data in view
                        txt_name.setText(name);
                        txt_tele.setText(Html.fromHtml("Teléfono:&nbsp&nbsp " + tel));
                        txt_mail.setText(Html.fromHtml("Email:&nbsp&nbsp " + email));
                        txt_desc.setText(de);

                        // Change Activity Title with Song title
                        setTitle(name);

                        findViewById(R.id.btnLocalizar).setOnClickListener(new ActionButton2());
                    }else{
                        if(imageBit!=null)
                        {
                            iv.setImageBitmap(imageBit);
                        }

                        TextView txt_name = (TextView) findViewById(R.id.name);
                        TextView txt_tele = (TextView) findViewById(R.id.phone);
                        TextView txt_mail = (TextView) findViewById(R.id.email);
                        TextView txt_desc = (TextView) findViewById(R.id.description);


                        TextView tit_Contacto = (TextView) findViewById(R.id.TitContacto);
                        Button localizar = (Button) findViewById(R.id.btnLocalizar);
                        Button btnagreExp = (Button) findViewById(R.id.btnAgreExpo);
                        btnagreExp.setText("ADD TO MY EXHIBITORS");
                        // displaying song data in view
                        txt_name.setText(name);
                        txt_tele.setText(Html.fromHtml("Phone:&nbsp&nbsp " + tel));
                        txt_mail.setText(Html.fromHtml("Mail:&nbsp&nbsp " + email));
                        txt_desc.setText(de);

                        tit_Contacto.setText("CONTACT INFORMATION");
                        localizar.setText("FIND ON MAP");

                        // Change Activity Title with Song title
                        setTitle(name);

                        findViewById(R.id.btnLocalizar).setOnClickListener(new ActionButton2());
                    }

                }
            });

        }
        private Bitmap downloadBitmap(String url) {
            Log.d("url img", url);
            // initilize the default HTTP client object
            final DefaultHttpClient client = new DefaultHttpClient();

            //forming a HttoGet request
            final HttpGet getRequest = new HttpGet(url);
            try {

                HttpResponse response = client.execute(getRequest);

                //check 200 OK for success
                final int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode != HttpStatus.SC_OK) {
                    Log.w("ImageDownloader", "Error " + statusCode +
                            " while retrieving bitmap from " + url);
                    return null;

                }

                final HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream inputStream = null;
                    try {
                        // getting contents from the stream
                        inputStream = entity.getContent();

                        // decoding stream data back into image Bitmap that android understands
                        imageBit = BitmapFactory.decodeStream(inputStream);


                    } finally {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        entity.consumeContent();
                    }
                }
            } catch (Exception e) {
                // You Could provide a more explicit error message for IOException
                getRequest.abort();
                Log.e("ImageDownloader", "Something went wrong while" +
                        " retrieving bitmap from " + url + e.toString());
            }

            return imageBit;
        }

    }

    class LoadBanners extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting Albums JSON
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("path", "publicidad"));
            // getting JSON string from URL
            String json = jsonParser.makeHttpRequest(URL_BANNERS, "GET",
                    params);

            // Check your log cat for JSON reponse
            Log.d("DATA JSON: ", "> " + json);

            try {
                albums = new JSONArray(json);

                if (albums != null) {
                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);


                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        String tabletSize = getResources().getString(R.string.screen_type);
                        Log.d("pantalla", tabletSize);
                        if ("phone".equals(tabletSize)) {
                            // do something
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_BANNER);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app-ecodsa.com.mx/publicidadc/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        } else {
                            // do something else
                            // Storing each json item values in variable
                            String id = c.getString(TAG_ID_BANNER);
                            String name = c.getString(TAG_NAME_TABLET);
                            String link = c.getString(TAG_BANNER_LINK);
                            // adding HashList to ArrayList
                            nombre = name ;
                            File file = new File(MEDIA_PATH_BANNER_TABLET, name);
                            if(!file.exists()) {
                                downloadImagesToSdCard("http://app-ecodsa.com.mx/publicidadt/" + name, name);
                            }
                            BannerList.add(name);
                            LinkBannerList.add(link);
                        }

                        //ImageView imgView = (ImageView)findViewById(R.id.Banner);

                    }
                }else{
                    Log.d("Data: ", "null");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all albums
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {


                    final String[] simpleArray = BannerList.toArray(new String[BannerList.size()]);
                    final String[] LinksimpleArray = LinkBannerList.toArray(new String[LinkBannerList.size()]);
                    imageButton = (ImageButton) findViewById(R.id.Banner);


                    final Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        int i=0;
                        public void run() {


                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int width = size.x;

                            String tabletSize = getResources().getString(R.string.screen_type);
                            Log.d("pantalla", tabletSize);
                            if ("phone".equals(tabletSize)) {
                                // do something
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/1080 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                // do something else
                                imageButton.setLayoutParams(new LinearLayout.LayoutParams(width, ( 230 * width )/2048 ) );
                                imageButton.setImageDrawable(Drawable.createFromPath(MEDIA_PATH_BANNER + simpleArray[i]));
                                imageButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent = intent.setData(Uri.parse(LinksimpleArray[i]));
                                        startActivity(intent);
                                    }
                                });
                            }

                            i++;
                            if(i>simpleArray.length-1)
                            {
                                i=0;
                            }
                            handler.postDelayed(this, 5000);  //for interval...
                        }
                    };
                    handler.postDelayed(runnable, 1000); //for initial delay..
                }
            });

        }



        private void downloadImagesToSdCard(String downloadUrl,String imageName)
        {
            try{
                URL url = new URL(downloadUrl); //you can write here any link

                File myDir =  new File(MEDIA_PATH_BANNER);
                //Something like ("/sdcard/file.mp3")


                if(!myDir.exists()){
                    myDir.mkdir();
                    Log.v("", "inside mkdir");

                }

                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = imageName;
                File file = new File(myDir, fname);
                if (file.exists ()) file.delete ();

             /* Open a connection to that URL. */
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection)ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

            /*
             * Define InputStreams to read from the URLConnection.
             */
                // InputStream is = ucon.getInputStream();
            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */

                FileOutputStream fos = new FileOutputStream(file);
                int size = 1024*1024;
                byte[] buf = new byte[size];
                int byteRead;
                while (((byteRead = inputStream.read(buf)) != -1)) {
                    fos.write(buf, 0, byteRead);
                    int bytesDownloaded = byteRead;
                }
            /* Convert the Bytes read to a String. */

                fos.close();

            }catch(IOException io)
            {
                io.printStackTrace();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

    }

}

