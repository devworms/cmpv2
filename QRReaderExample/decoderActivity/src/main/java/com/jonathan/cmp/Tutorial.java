package com.jonathan.cmp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Locale;

/**
 * Created by salva on 16/05/2016.
 */
public class Tutorial extends Activity {
    String ID,tabla,TAG_PROGRAMA,idioma;
    SharedPreferences sp;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutoriales);

        sp = getSharedPreferences("prefe", Activity.MODE_PRIVATE);
        TextView txtMensaje=(TextView)findViewById(R.id.txtTutorial);
        TextView txtTitulo=(TextView)findViewById(R.id.txtTituloTuto);
        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        TAG_PROGRAMA= i.getStringExtra("TITULO");
        tabla = i.getStringExtra("tabla");
        idioma = i.getStringExtra("idio");
        if(tabla.equals("program")){
            txtTitulo.setText("TUTORIAL CONFERENCIAS");
            txtMensaje.setText("Debes seleccionar primero, el día que te interesa, después se activaran las opciones Tipo y Salas, para completar la búsqueda");
        }else if(tabla.equals("patrocinador")){
            txtTitulo.setText("TUTORIAL PATROCINADORES");
            txtMensaje.setText("Selecciona una de las siguientes secciones y recibe la información del patrocinador de tu intéres");
        }else if(tabla.equals("expositor")){
            txtTitulo.setText("TUTORIAL EXPOSICIÓN INDUSTRIAL");
            txtMensaje.setText("Escribe en el campo de búsqueda el nombre del expositor de tu interés y encuéntralo fácilmente. Agrégalo a Mis Expositores");
        }else if(tabla.equals("deportivo")){
            txtTitulo.setText("TUTORIAL PROGRAMA SOCIAL & DEPORTIVO");
            txtMensaje.setText("Selecciones su evento social o deportivo de interes. Puedes agregarlo a Mi Agenda");
        }else if(tabla.equals("acompanante")){
            txtTitulo.setText("TUTORIAL ACOMPAÑANTES");
            txtMensaje.setText("Selecciones su evento de acompañante de interes. Puedes agregarlo a Mi Agenda");
        }else if(tabla.equals("inscripciones")){
            txtTitulo.setText("TUTORIAL INSCRIPCIÓN Y HOSPEDAJE");
            txtMensaje.setText("Selecciona el botón de “Inscripción” y obtén la información necesaria para aclarar tus dudas");
        } else if(tabla.equals("SCANNER")){
            txtTitulo.setText("TUTORIAL SCANNER");
            txtMensaje.setText("Activa el scanner, colócalo en el código QR y guarda los datos de tus contactos del CMP");
        } else if(tabla.equals("MAPAS")){
            txtTitulo.setText("TUTORIAL MAPA");
            txtMensaje.setText("Selecciona el mapa que deseas ver,\nMapa del Recindto:CINTERMEX,\nMapa Expositores:En este mapa se mostrara en donde se ubican los expositores");

        }else if(tabla.equals("Foto")){
            txtTitulo.setText("TUTORIAL FOTOS");
            txtMensaje.setText("Descarga las fotos del evento del CMP");

        } else if(tabla.equals("encuesta")){
            txtTitulo.setText("TUTORIAL ENCUESTAS");
            txtMensaje.setText("Elige una de las conferencias a las que asististe y califícala");

        } else if(tabla.equals("descargas")){
            txtTitulo.setText("TUTORIAL DESCARGAS");
            txtMensaje.setText("Visualiza los archivos más importantes respecto al evento");

        }  else if(tabla.equals("MisExp")){
            txtTitulo.setText("TUTORIAL MIS EXPOSITORES");
            txtMensaje.setText("En esta sección se guardaran los que hayas agregado de Expositores Industriales");

        } else if(tabla.equals("Agenda")){
            txtTitulo.setText("TUTORIAL AGENDA");
            txtMensaje.setText("En esta sección podrás visualizar los eventos guardados para recibir alertas antes de que inicien");

        }  else if(tabla.equals("SitInt")){
            txtTitulo.setText("TUTORIAL SITIOS DE INTERÉS");
            txtMensaje.setText("Encontrarás información de sitios turísticos y teléfonos de emergencia.");

        }else{
            txtTitulo.setText("TUTORIAL TRANSPORTACIÓN");
            txtMensaje.setText("Encontrarás información de las rutas y horarios de traslado a todos los eventos del CMP");

        }

        findViewById(R.id.btnOkTuto).setOnClickListener(new ActionButton1());


    }
    class ActionButton1 implements View.OnClickListener {
        public void onClick(View v) {
            if(tabla.equals("program")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Conferencia", "si");
                editor.commit();

                Intent intent = new Intent(getApplicationContext(), FiltroProgramEncuesta.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                intent.putExtra("tabla", tabla);
                intent.putExtra("idio",idioma);
                startActivity(intent);
                finish();
            }else if(tabla.equals("patrocinador")){
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Patrocinadores", "si");
                editor.commit();

                Intent intent = new Intent(getApplicationContext(), Patrocinadores.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                intent.putExtra("tabla", tabla);
                intent.putExtra("idio",idioma);
                startActivity(intent);
                finish();
            }else if(tabla.equals("expositor")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("ExpoIndu", "si");
                editor.commit();

                Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                intent.putExtra("tabla", tabla);
                startActivity(intent);
                finish();
            }else if(tabla.equals("deportivo")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("SociDepor", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), ProgramSociaDepor.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                intent.putExtra("tabla", tabla);
                startActivity(intent);
                finish();
            }else if(tabla.equals("acompanante")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Acomp", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), ProgramSociaDepor.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                intent.putExtra("tabla", tabla);
                startActivity(intent);
                finish();
            }else if(tabla.equals("inscripciones")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("InsHosp", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), HospeInsc.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                intent.putExtra("tabla", tabla);
                startActivity(intent);
                finish();
            }else if(tabla.equals("SCANNER")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Scanner", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), SimpleScannerActivity.class);
                startActivity(intent);
                finish();
            }else if(tabla.equals("MAPAS")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Mapa", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), FiltroMapa.class);
                intent.putExtra("TITULO", TAG_PROGRAMA);
                startActivity(intent);
                finish();
            }else if(tabla.equals("Foto")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Foto", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), MenuPhoto.class);
                startActivity(intent);
                finish();
            }else if(tabla.equals("encuesta")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Encu", "si");
                editor.commit();
                String LENGUAJE = Locale.getDefault().getDisplayLanguage();
                if ("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
                    Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
                    intent.putExtra("TITULO", TAG_PROGRAMA);
                    intent.putExtra("tabla", tabla);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), ListadoActivity.class);
                    intent.putExtra("TITULO", TAG_PROGRAMA);
                    intent.putExtra("tabla", tabla);
                    startActivity(intent);
                }
                finish();
            }else if(tabla.equals("descargas")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Desc", "si");
                editor.commit();
                String LENGUAJE = Locale.getDefault().getDisplayLanguage();
                if ("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
                    Intent intent = new Intent(getApplicationContext(), CursosPreCongre.class);

                    startActivity(intent);
                }
                finish();
            }else if(tabla.equals("MisExp")){

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("MisExp", "si");
                editor.commit();
                String LENGUAJE = Locale.getDefault().getDisplayLanguage();
                Intent intent = new Intent(getApplicationContext(), Buscar.class);
                LENGUAJE = Locale.getDefault().getDisplayLanguage();
                if ("Español".equals(LENGUAJE) || "español".equals(LENGUAJE)) {
                    intent.putExtra("TITULO", "MIS EXPOSITORES");
                } else {
                    intent.putExtra("TITULO", "MY EXHIBITORS");
                }
                startActivity(intent);
                finish();
            }else if(tabla.equals("Agenda")){
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Agenda", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), Agenda.class);
                startActivity(intent);
                finish();
            }else if(tabla.equals("SitInt")){
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("SitInt", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), Sitios_Instrst.class);
                startActivity(intent);
                finish();
            }else{
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("Trans", "si");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), Transportacion.class);
                startActivity(intent);
                finish();
            }




        }
    }
}
